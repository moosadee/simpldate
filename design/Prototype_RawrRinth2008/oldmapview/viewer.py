import pygame, sys
from pygame.locals import *

pygame.init()

window = pygame.display.set_mode( ( 1280, 720 ) )
pygame.display.set_caption( "Map viewer" )

tileset = pygame.image.load( "tileset2.png" )

mapfile = open( "lv-00.map", "r" )

layers = []
# layers.append( [] )
# layers.append( [] )
# layers.append( [] )

width = 80
height = 60
tilewh = 20
x = 0
y = 0
layer = 0

for line in mapfile:
    items = line.split()
    print( len( items ) )
    for word in items:

        if ( word != "0000" ):
            tile = {}
            tile["x"] = x
            tile["y"] = y
            tile["type"] = int( word )
            layers.append( tile )

        x += 1
        if ( x >= width ):
            x = 0
            y += 1

        if ( y >= height ):
            x = 0
            y = 0
            layer += 1


cameraX = 0
cameraY = 0
cameraSpeed = 20

while ( True ):
    window.fill( pygame.Color( 255, 255, 255 ) )

    # Draw map
    for tile in layers:
        # blit(source, dest, area=None, special_flags=0) -> Rect
        frameRect = Rect( tile["type"] * tilewh, 0, tilewh, tilewh )
        destinationRect = Rect( tile["x"] * tilewh - cameraX, tile["y"] * tilewh - cameraY, tilewh, tilewh )
        window.blit( tileset, destinationRect, frameRect )

    for event in pygame.event.get():
        if ( event.type == QUIT ):
            pygame.quit()
            sys.exit()

    keys = pygame.key.get_pressed()

    if ( keys[ K_DOWN ] ): cameraY += cameraSpeed
    elif ( keys[ K_UP ] ): cameraY -= cameraSpeed

    if ( keys[ K_RIGHT ] ): cameraX += cameraSpeed
    elif ( keys[ K_LEFT ] ): cameraX -= cameraSpeed

    pygame.display.update()
