#include "Utilities.h"

bool BoundingBoxCollision( struct Rect rect1, struct Rect rect2 )
{
    return (
        rect1.x           < rect2.x + rect2.w &&
        rect1.x + rect1.w > rect2.x           &&
        rect1.y           < rect2.y + rect2.h &&
        rect1.y + rect1.h > rect2.y
        );
}

void SetRandomScreenPosition( struct SYSTEM* sys, struct Rect* rect )
{
    rect->x = rand() % ( sys->renderBounds.w - rect->w );
    rect->y = rand() % ( sys->renderBounds.h - rect->h );
}

struct Rect GetRect( int x, int y, int w, int h )
{
    struct Rect rect;
    rect.x = x;
    rect.y = y;
    rect.w = w;
    rect.h = h;
    return rect;
}

bool PointInBox( int x, int y, struct Rect box )
{
    return (  box.x <= x && x <= box.x + box.w &&
              box.y <= y && y <= box.y + box.h
        );
}

struct Rect GetOffsetRect( struct Rect original, struct Rect offset )
{
    struct Rect newRect;
    newRect.x = original.x - offset.x;
    newRect.y = original.y - offset.y;
    newRect.w = original.w;
    newRect.h = original.h;
    return newRect;
}
