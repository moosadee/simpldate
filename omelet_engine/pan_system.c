#include "pan_system.h"
#include "Structures.h"

#include <stdio.h>
#include <stdlib.h>

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#include "pd_system.h"
#elif defined(TARGET_SDL)
#endif

/**
   @param      sys                 Pointer to the SYSTEM struct
   @param      renderWidth         Width of the base screen size
   @param      renderHeight        Height of the base screen size
   @param      windowWidth         Width of the actual window size (for Playdate, will be overwritten by renderWidth)
   @param      windowHeight        Height of the actual window size (for Playdate, will be overwritten by renderHeight)
   @param      windowTitle         Title to draw at the top of the window
   @param      hardwareRendering   Whether to use software rendering (false) or hardware rendering (true) - (SDL)
   Initializes game libraries based on platform
*/
void Pan_Setup( struct SYSTEM* sys, int renderWidth, int renderHeight, int windowWidth, int windowHeight, const char* windowTitle, bool hardwareRendering )
{
    Pan_LogSetup( sys, __FILE__, __func__, __LINE__, MINIMAL );
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

    sys->fatalError = false;
    sys->wantToQuit = false;
    sys->logLevel = MINIMAL;
    strcpy( sys->nextState, "TitleState" ); // "" for no change
//  strcpy( sys->nextState, "MapEditor_State" ); // "" for no change
    sys->renderBounds.x = 0;
    sys->renderBounds.y = 0;
    sys->renderBounds.w = renderWidth;
    sys->renderBounds.h = renderHeight;
    sys->windowBounds.x = 0;
    sys->windowBounds.y = 0;
    sys->windowBounds.w = windowWidth;
    sys->windowBounds.h = windowHeight;

    sys->scaleRatio.x = sys->scaleRatio.y = 0; // Not used
    sys->scaleRatio.w = sys->windowBounds.w / sys->renderBounds.w;
    sys->scaleRatio.h = sys->windowBounds.h / sys->renderBounds.h;

    sys->globalInterval.current = 0;
    sys->globalInterval.increment = 0.1;
    sys->globalInterval.maximum = 4;

    sys->currentState.Setup        = NULL;
    sys->currentState.Teardown     = NULL;
    sys->currentState.HandleInput  = NULL;
    sys->currentState.Update       = NULL;
    sys->currentState.Draw         = NULL;
    sys->currentState.CycleBegin   = NULL;
    sys->currentState.CycleEnd     = NULL;


#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    sys->pd_soundplayer = sys->playdate->sound->sampleplayer->newPlayer();
    sys->pd_fileplayer = sys->playdate->sound->fileplayer->newPlayer();

    sys->windowBounds.w = sys->renderBounds.w;
    sys->windowBounds.h = sys->renderBounds.h;
#elif defined(TARGET_SDL)
    sys->fpsTimer.startTicks = SDL_GetTicks();
    sys->capTimer.startTicks = 0;
    sys->fps = 60;
    sys->ticksPerFrame = 1000 / sys->fps;

    sys->sdl_window = NULL;
    sys->sdl_surface = NULL;
    sys->sdl_renderer = NULL;

    sys->mouseDown = false;

    sys->hardwareRendering = hardwareRendering;
    if ( sys->hardwareRendering )
    {
        Pan_LogText( sys, __FILE__, __func__, __LINE__, MINIMAL, "Hardware rendering ON" );
    }
    else
    {
        Pan_LogText( sys, __FILE__, __func__, __LINE__, MINIMAL, "Hardware rendering OFF" );
    }

    Pan_LogText( sys, __FILE__, __func__, __LINE__, MINIMAL, "SDL_Init" );
    if ( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
        Pan_LogError( sys, __FILE__, __func__, __LINE__, MINIMAL, "SDL_Init", SDL_GetError() );
        sys->fatalError = true;
        return;
    }
    else { /* printf( "o %s:%i SDL_Init successful \n", __FILE__, __LINE__ ); */ }

    Pan_LogText( sys, __FILE__, __func__, __LINE__, MINIMAL, "TTF_Init" );
    if ( TTF_Init() == -1 )
    {
        Pan_LogError( sys, __FILE__, __func__, __LINE__, MINIMAL, "TTF_Init", TTF_GetError() );
        sys->fatalError = true;
        return;
    }
    else { /* printf( "o %s:%i TTF_Init successful \n", __FILE__, __LINE__ ); */ }

    Pan_LogText( sys, __FILE__, __func__, __LINE__, MINIMAL, "SDL_CreateWindow" );
    sys->sdl_window = SDL_CreateWindow( windowTitle, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, sys->windowBounds.w, sys->windowBounds.h, SDL_WINDOW_SHOWN );
    if ( sys->sdl_window == NULL )
    {
        Pan_LogError( sys,__FILE__, __func__, __LINE__, MINIMAL, "SDL_CreateWindow", SDL_GetError() );
        sys->fatalError = true;
        return;
    }
    else { /* printf( "o %s:%i SDL_CreateWindow successful \n", __FILE__, __LINE__ ); */ }

    if ( sys->hardwareRendering )
    {
        Pan_LogText( sys, __FILE__, __func__, __LINE__, MINIMAL, "SDL_CreateRenderer" );
        sys->sdl_renderer = SDL_CreateRenderer( sys->sdl_window, -1, SDL_RENDERER_ACCELERATED );
        if ( sys->sdl_renderer == NULL )
        {
            Pan_LogError( sys, __FILE__, __func__, __LINE__, MINIMAL, "SDL_CreateRenderer", SDL_GetError() );
            sys->fatalError = true;
            return;
        }
        SDL_SetRenderDrawColor( sys->sdl_renderer, 0xFF, 0xFF, 0xFF, 0xFF );
        SDL_RenderSetLogicalSize( sys->sdl_renderer, sys->renderBounds.w, sys->renderBounds.h );
        SDL_RenderSetScale( sys->sdl_renderer, sys->scaleRatio.w, sys->scaleRatio.h );
    }
    else
    {
        Pan_LogText( sys, __FILE__, __func__, __LINE__, MINIMAL, "SDL_GetWindowSurface" );
        sys->sdl_surface = SDL_GetWindowSurface( sys->sdl_window );
        if ( sys->sdl_surface == NULL )
        {
            Pan_LogError( sys, __FILE__, __func__, __LINE__, MINIMAL, "SDL_GetWindowSurface", SDL_GetError() );
            sys->fatalError = true;
            return;
        }
    }

    Pan_LogText( sys, __FILE__, __func__, __LINE__, MINIMAL, "IMG_Init" );
    int imgFlags = IMG_INIT_PNG;
    if ( !( IMG_Init( imgFlags ) & imgFlags ) )
    {
        Pan_LogError( sys, __FILE__, __func__, __LINE__, MINIMAL, "IMG_Init", IMG_GetError() );
        sys->fatalError = true;
        return;
    }

    Pan_LogText( sys, __FILE__, __func__, __LINE__, MINIMAL, "Mix_OpenAudio" );
    if ( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0 )
    {
        Pan_LogError( sys, __FILE__, __func__, __LINE__, MINIMAL, "SDL_mixer", Mix_GetError() );
        sys->fatalError = true;
        return;
    }

    sys->input.buttonLeft    = INACTIVE;
    sys->input.buttonRight   = INACTIVE;
    sys->input.buttonUp      = INACTIVE;
    sys->input.buttonDown    = INACTIVE;
    sys->input.buttonActionL = INACTIVE;
    sys->input.buttonActionR = INACTIVE;
#endif

    sys->musicVolume.maximum = 128;
    sys->musicVolume.current = sys->musicVolume.maximum/2;
    sys->musicVolume.increment = sys->musicVolume.maximum/10;
    sys->soundVolume.maximum = 128;
    sys->soundVolume.current = sys->soundVolume.maximum/2;
    sys->soundVolume.increment = sys->soundVolume.maximum/10;

    Pan_MusicVolume( sys, sys->musicVolume.current );
    Pan_AllSoundVolume( sys, sys->soundVolume.current );
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}

void Pan_Teardown( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    sys->playdate->sound->sampleplayer->freePlayer( sys->pd_soundplayer );
    sys->playdate->sound->fileplayer->freePlayer( sys->pd_fileplayer );
#elif defined(TARGET_SDL)
    if ( sys->hardwareRendering )
    {
        SDL_DestroyRenderer( sys->sdl_renderer );
    }
    SDL_DestroyWindow( sys->sdl_window );
    TTF_Quit();
    IMG_Quit();
    Mix_Quit();
    SDL_Quit();
    Pan_LogText( sys, __FILE__, __func__, __LINE__, MINIMAL, "PROGRAM ENDS SAFELY" );
#endif
    Pan_LogTeardown( sys, __FILE__, __func__, __LINE__ );
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}

void Pan_ClearScreen( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    sys->playdate->graphics->clear( kColorWhite );
#elif defined(TARGET_SDL)
    if ( sys->hardwareRendering )
    {
        SDL_RenderClear( sys->sdl_renderer );
    }
    else
    {
        SDL_FillRect( sys->sdl_surface, NULL, SDL_MapRGB( sys->sdl_surface->format, 0xCC, 0xCC, 0xCC ) );
    }
#endif
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}

void Pan_DrawScreen( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
    if ( sys->hardwareRendering )
    {
        SDL_RenderPresent( sys->sdl_renderer );
    }
    else
    {
        SDL_UpdateWindowSurface( sys->sdl_window );
    }
#endif
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}

void Pan_HandleInput( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
    SDL_Event e;
    sys->mouseDown = false;
    while ( SDL_PollEvent( &e ) )
    {
        if ( e.type == SDL_QUIT ) {
            sys->wantToQuit = true;
        }

        else if ( e.type == SDL_MOUSEBUTTONDOWN )
        {
            // TODO: Add the ability to click MenuButton items?
            SDL_GetMouseState( &sys->mouseX, &sys->mouseY );
            sys->mouseX /= sys->scaleRatio.w;
            sys->mouseY /= sys->scaleRatio.h;
            sys->mouseDown = true;
        }
        else if ( e.type == SDL_MOUSEMOTION )
        {
            SDL_GetMouseState( &sys->mouseX, &sys->mouseY );
            sys->mouseX /= sys->scaleRatio.w;
            sys->mouseY /= sys->scaleRatio.h;
        }

        else if ( e.type == SDL_KEYDOWN )
        {
            // INACTIVE -> BEGIN_PUSH -> HELD_DOWN
            if      ( e.key.keysym.sym == SDLK_w && sys->input.buttonUp      == INACTIVE   ) { sys->input.buttonUp    = BEGIN_PUSH;   }
            else if ( e.key.keysym.sym == SDLK_w && sys->input.buttonUp      == BEGIN_PUSH ) { sys->input.buttonUp    = HELD_DOWN;    }

            if      ( e.key.keysym.sym == SDLK_s && sys->input.buttonDown    == INACTIVE   ) { sys->input.buttonDown  = BEGIN_PUSH;   }
            else if ( e.key.keysym.sym == SDLK_s && sys->input.buttonDown    == BEGIN_PUSH ) { sys->input.buttonDown  = HELD_DOWN;    }

            if      ( e.key.keysym.sym == SDLK_a && sys->input.buttonLeft    == INACTIVE   ) { sys->input.buttonLeft  = BEGIN_PUSH;   }
            else if ( e.key.keysym.sym == SDLK_a && sys->input.buttonLeft    == BEGIN_PUSH ) { sys->input.buttonLeft  = HELD_DOWN;    }

            if      ( e.key.keysym.sym == SDLK_d && sys->input.buttonRight   == INACTIVE   ) { sys->input.buttonRight = BEGIN_PUSH;   }
            else if ( e.key.keysym.sym == SDLK_d && sys->input.buttonRight   == BEGIN_PUSH ) { sys->input.buttonRight = HELD_DOWN;    }

            if      ( e.key.keysym.sym == SDLK_j && sys->input.buttonActionL == INACTIVE   ) { sys->input.buttonActionL = BEGIN_PUSH; }
            else if ( e.key.keysym.sym == SDLK_j && sys->input.buttonActionL == BEGIN_PUSH ) { sys->input.buttonActionL = HELD_DOWN;  }

            if      ( e.key.keysym.sym == SDLK_k && sys->input.buttonActionR == INACTIVE   ) { sys->input.buttonActionR = BEGIN_PUSH; }
            else if ( e.key.keysym.sym == SDLK_k && sys->input.buttonActionR == BEGIN_PUSH ) { sys->input.buttonActionR = HELD_DOWN;  }
        }
        else if ( e.type == SDL_KEYUP )
        {
            // HELD_DOWN -> BEGIN_RELEASE -> INACTIVE
            // BEGIN_PUSH -> BEGIN_RELEASE
            if      ( e.key.keysym.sym == SDLK_w ) { sys->input.buttonUp      = BEGIN_RELEASE; }
            if      ( e.key.keysym.sym == SDLK_s ) { sys->input.buttonDown    = BEGIN_RELEASE; }
            if      ( e.key.keysym.sym == SDLK_a ) { sys->input.buttonLeft    = BEGIN_RELEASE; }
            if      ( e.key.keysym.sym == SDLK_d ) { sys->input.buttonRight   = BEGIN_RELEASE; }
            if      ( e.key.keysym.sym == SDLK_j ) { sys->input.buttonActionL = BEGIN_RELEASE; }
            if      ( e.key.keysym.sym == SDLK_k ) { sys->input.buttonActionR = BEGIN_RELEASE; }

            // Playdate has a menu where you can go back, so ESC will take you back to the main menu here.
            if      ( e.key.keysym.sym == SDLK_ESCAPE ) { strcpy( sys->nextState, "TitleState" ); }
        }
    }
#endif
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}

void Pan_Update( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

    Pan_IncrementCounter( &sys->globalInterval );

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
    if ( sys->input.buttonUp       == BEGIN_RELEASE ) { sys->input.buttonUp      = INACTIVE; }
    if ( sys->input.buttonDown     == BEGIN_RELEASE ) { sys->input.buttonDown    = INACTIVE; }
    if ( sys->input.buttonLeft     == BEGIN_RELEASE ) { sys->input.buttonLeft    = INACTIVE; }
    if ( sys->input.buttonRight    == BEGIN_RELEASE ) { sys->input.buttonRight   = INACTIVE; }
    if ( sys->input.buttonActionL  == BEGIN_RELEASE ) { sys->input.buttonActionL = INACTIVE; }
    if ( sys->input.buttonActionR  == BEGIN_RELEASE ) { sys->input.buttonActionR = INACTIVE; }
#endif
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}

void Pan_CycleBegin( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
    sys->capTimer.startTicks = SDL_GetTicks();
#endif
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}

void Pan_CycleEnd( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
    sys->capTimer.frameTicks = SDL_GetTicks() - sys->capTimer.startTicks;
    if ( sys->capTimer.frameTicks < sys->ticksPerFrame )
    {
        SDL_Delay( sys->ticksPerFrame - sys->capTimer.frameTicks );
    }
#endif
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}

void Pan_IncrementCounter( struct Counter* counter )
{
    counter->current += counter->increment;
    if ( counter->current >= counter->maximum )
    {
        counter->current = 0;
    }
}

void Pan_LogSetup( struct SYSTEM* sys, const char* file, const char* func, int line, enum LogLevel logLevel )
{
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    sys->playdate->system->logToConsole( "LOGGING BEGINS [%s:%s:%d]", file, func, line );
#elif defined(TARGET_SDL)
    FILE* logfile;
    logfile = fopen( "log.html", "w" );
    fprintf( logfile, "<style type='text/css'> \n" );
    fprintf( logfile, "tr:nth-child(odd) { background: #eee; } \n" );
    fprintf( logfile, "tr { border-collapse: collapse; border: solid 1px #000; } \n" );
    fprintf( logfile, "table { width:100%; border-collapse: collapse; border: solid 1px #000; font-family: monospace; font-size: 12pt; } \n" );
    fprintf( logfile, "</style> \n" );
    fprintf( logfile, "<table><tr><th>Message</th><th>File</th><th>Function</th><th>Line</th><tr>\n" );
    fprintf( logfile, "<tr><td> LOGGING BEGINS </td><td> %s </td><td> %s </td><td> %d </td></tr> \n", file, func, line );
    fclose( logfile );
#endif
}

void Pan_LogTeardown         ( struct SYSTEM* sys, const char* file, const char* func, int line )
{
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    sys->playdate->system->logToConsole( "LOGGING ENDS [%s:%s:%d]", file, func, line );
#elif defined(TARGET_SDL)
    FILE* logfile;
    logfile = fopen( "log.html", "a" );
    fprintf( logfile, "<tr><td> LOGGING ENDS </td><td> %s </td><td> %s </td><td> %d </td></tr> \n", file, func, line );
    fprintf( logfile, "</table> \n" );
    fclose( logfile );
#endif
}

void Pan_LogText( struct SYSTEM* sys, const char* file, const char* func, int line, enum LogLevel logLevel, const char* text )
{
    if ( logLevel > sys->logLevel ) { return; }
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    sys->playdate->system->logToConsole( "%s [%s:%s:%d]", text, file, func, line );
#elif defined(TARGET_SDL)
    FILE* logfile;
    logfile = fopen( "log.html", "a" );
    fprintf( logfile, "<tr><td> %s </td><td> %s </td><td> %s </td><td> %d </td></tr> \n", text, file, func, line );
    fclose( logfile );
#endif
}

void Pan_LogValueString( struct SYSTEM* sys, const char* file, const char* func, int line, enum LogLevel logLevel, const char* label, const char* value )
{
    if ( logLevel > sys->logLevel ) { return; }
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    sys->playdate->system->logToConsole( "%s = %s [%s:%s:%d]", label, value, file, func, line );
#elif defined(TARGET_SDL)
    FILE* logfile;
    logfile = fopen( "log.html", "a" );
    fprintf( logfile, "<tr><td> %s = %s </td><td> %s </td><td> %s </td><td> %d </td></tr> \n", label, value, file, func, line );
    fclose( logfile );
#endif
}

void Pan_LogValueInt( struct SYSTEM* sys, const char* file, const char* func, int line, enum LogLevel logLevel, const char* label, int value )
{
    if ( logLevel > sys->logLevel ) { return; }
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    sys->playdate->system->logToConsole( "%s = %d [%s:%s:%d]", label, value, file, func, line );
#elif defined(TARGET_SDL)
    FILE* logfile;
    logfile = fopen( "log.html", "a" );
    fprintf( logfile, "<tr><td> %s = %d </td><td> %s </td><td> %s </td><td> %d </td></tr> \n", label, value, file, func, line );
    fclose( logfile );
#endif
}

void Pan_LogFunctionBeginEnd( struct SYSTEM* sys, const char* file, const char* func, int line, const char* text )
{
    if ( sys->logLevel < VERBOSE ) { return; }
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    sys->playdate->system->logToConsole( "%s [%s:%s:%d]", text, file, func, line );
#elif defined(TARGET_SDL)
    FILE* logfile;
    logfile = fopen( "log.html", "a" );
    fprintf( logfile, "<tr><td> %s </td><td> %s </td><td> %s </td><td> %d </td></tr> \n", text, file, func, line );
    fclose( logfile );
#endif
}

void Pan_LogError( struct SYSTEM* sys, const char* file, const char* func, int line, enum LogLevel logLevel, const char* text, const char* error )
{
    if ( logLevel > sys->logLevel ) { return; }
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    sys->playdate->system->logToConsole( "ERROR: %s (%s) [%s:%s:%d]", error, text, file, func, line );
#elif defined(TARGET_SDL)
    FILE* logfile;
    logfile = fopen( "log.html", "a" );
    fprintf( logfile, "<tr><td> ERROR: %s (%s) </td><td> %s </td><td> %s </td><td> %d </td></tr> \n", error, text, file, func, line );
    fclose( logfile );
#endif
}

//                                                                                            --------
//                                                                                            - TEXT -
//----------------------------------------------------------------------------------------------------
/**
   @param    sys     The SYSTEM struct pointer
   @param    path    Path to the font file to load
   @param    size    (SDL ONLY) the size to set the font as
*/
struct Font Pan_TryLoadFont( struct SYSTEM* sys, const char* path, int size, const char* ext )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    Pan_LogValueString( sys, __FILE__, __func__, __LINE__, MINIMAL, "Path", path );
    struct Font font;
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    const char* err;
    font.table = sys->playdate->graphics->loadBitmapTable( path, &err );
    if ( font.table == NULL )
    {
        sys->playdate->system->error( "%s:%i loadBitmapTable %s: %s", __FILE__, __LINE__, path, err );
    }
#elif defined(TARGET_SDL)
    if ( sys->hardwareRendering == false )
    {
        Pan_LogError( sys, __FILE__, __func__, __LINE__, MINIMAL, "Calling Pan_TryLoadFont; not available when hardware rendering is off! Use Image instead!", ":(" );
//      return;
    }

    char fullpath[100];
    sprintf( fullpath, "%s-table-%d-%d.%s", path, size, size, ext );
    font.table = Pan_TryLoadTexture( sys, fullpath ).texture;
#endif

    font.frameRect.x = 0;
    font.frameRect.y = 0;
    font.frameRect.w = size;
    font.frameRect.h = size;

    return font;
}

void Pan_FreeFont( struct SYSTEM* sys, struct Font* font )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
    SDL_DestroyTexture( font->table );
#endif
}

void Pan_DrawText( struct SYSTEM* sys, struct Font* font, const char* text, struct Rect* destination )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    int textLength = strlen( text );
    struct Rect letterPosition;
    letterPosition.x = destination->x;
    letterPosition.y = destination->y;
    letterPosition.w = font->frameRect.w;
    letterPosition.h = font->frameRect.h;

    struct Rect clipFrame = font->frameRect;
    for ( int i = 0; i < textLength; i++ )
    {
        int letterIndex = text[i] - 32;
        if ( text[i] == 0 )
        {
            letterIndex = 0;
        }
        clipFrame.x = letterIndex * letterPosition.w;

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
        LCDBitmap* currentFrame = sys->playdate->graphics->getTableBitmap( font->table, letterIndex );
        sys->playdate->graphics->drawBitmap( currentFrame, letterPosition.x, letterPosition.y, 0 );
#elif defined(TARGET_SDL)
        SDL_RenderCopy( sys->sdl_renderer, font->table, &clipFrame, &letterPosition );
#endif

        letterPosition.x += clipFrame.w;
    }
}


//                                                                                           ---------
//                                                                                           - IMAGE -
//----------------------------------------------------------------------------------------------------
struct Image Pan_TryLoadImage( struct SYSTEM* sys, const char* path )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    Pan_LogValueString( sys, __FILE__, __func__, __LINE__, MINIMAL, "Path", path );
    struct Image image;
    image.image = NULL;
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    const char* err;
    image.image = sys->playdate->graphics->loadBitmap( path, &err );
    if ( image.image == NULL )
    {
        sys->playdate->system->error( "%s:%i loadBitmap %s: %s", __FILE__, __LINE__, path, err );
    }
#elif defined(TARGET_SDL)
    if ( sys->hardwareRendering )
    {
        Pan_LogError( sys, __FILE__, __func__, __LINE__, MINIMAL, "Calling Pan_TryLoadImage; not available when hardware rendering is on! Use Texture instead!", ":(" );
//      return;
    }

    Pan_LogText( sys, __FILE__, __func__, __LINE__, MINIMAL, "IMG_Load" );
    SDL_Surface* load = IMG_Load( path );
    if ( load == NULL )
    {
        Pan_LogError( sys, __FILE__, __func__, __LINE__, MINIMAL, "IMG_Load", IMG_GetError() );
        Pan_LogValueString( sys, __FILE__, __func__, __LINE__, MINIMAL, "Path", path );
    }
    else
    {
        Pan_LogText( sys, __FILE__, __func__, __LINE__, MINIMAL, "SDL_ConvertSurface" );
        image.image = SDL_ConvertSurface( load, sys->sdl_surface->format, 0 );
        if ( image.image == NULL )
        {
            Pan_LogError( sys, __FILE__, __func__, __LINE__, MINIMAL, "SDL_ConvertSurface", SDL_GetError() );
            Pan_LogValueString( sys, __FILE__, __func__, __LINE__, MINIMAL, "Path", path );
        }
        Pan_LogText( sys, __FILE__, __func__, __LINE__, MINIMAL, "SDL_FreeSurface" );
        SDL_FreeSurface( load );
        Pan_LogText( sys, __FILE__, __func__, __LINE__, MINIMAL, "SDL_SetColorKey" );
        SDL_SetColorKey( image.image, SDL_TRUE, SDL_MapRGB( image.image->format, 0xFF, 0, 0xFF ) );
    }
#endif

    return image;
}

void Pan_FreeImage( struct SYSTEM* sys, struct Image* image )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    sys->playdate->graphics->freeBitmap( image->image );
#elif defined(TARGET_SDL)
    SDL_FreeSurface( image->image );
#endif
}

void Pan_DrawImage( struct SYSTEM* sys, struct Image* image, int x, int y )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    sys->playdate->graphics->drawBitmap( image->image, x, y, 0 );
#elif defined(TARGET_SDL)
    Rect pos;
    pos.x = x;
    pos.y = y;
    SDL_BlitSurface( image->image, NULL, sys->sdl_surface, &pos );
#endif
}


//                                                                                     ---------------
//                                                                                     - IMAGE TABLE -
//----------------------------------------------------------------------------------------------------
struct ImageTable Pan_TryLoadImageTable( struct SYSTEM* sys, const char* path, int frameWidth, int frameHeight, const char* ext )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    Pan_LogValueString( sys, __FILE__, __func__, __LINE__, MINIMAL, "Path", path );
    struct ImageTable imageTable;
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    const char* err;
    imageTable.imageTable = sys->playdate->graphics->loadBitmapTable( path, &err );
    if ( imageTable.imageTable == NULL )
    {
        sys->playdate->system->error( "%s:%i loadBitmapTable %s: %s", __FILE__, __LINE__, path, err );
    }
#elif defined(TARGET_SDL)
    if ( sys->hardwareRendering )
    {
        Pan_LogError( sys, __FILE__, __func__, __LINE__, MINIMAL, "ERROR: Calling Pan_TryLoadImageTable; not available when hardware rendering is on! Use TextureTable instead!", ":(" );
//      return;
    }

    char fullpath[100];
    sprintf( fullpath, "%s-table-%d-%d.%s", path, frameWidth, frameHeight, ext );
    imageTable.imageTable = Pan_TryLoadImage( sys, fullpath ).image;
#endif
    imageTable.frameRect.x = 0;
    imageTable.frameRect.y = 0;
    imageTable.frameRect.w = frameWidth;
    imageTable.frameRect.h = frameHeight;
    return imageTable;
}

void Pan_FreeImageTable( struct SYSTEM* sys, struct ImageTable* imageTable )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    sys->playdate->graphics->freeBitmapTable( imageTable->imageTable );
#elif defined(TARGET_SDL)
    SDL_FreeSurface( imageTable->imageTable );
#endif
}

void Pan_DrawImageTable( struct SYSTEM* sys, struct ImageTable* imageTable, int x, int y, int frameIndex )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    LCDBitmap* currentFrame = sys->playdate->graphics->getTableBitmap( imageTable->imageTable, frameIndex );
    sys->playdate->graphics->drawBitmap( currentFrame, x, y, 0 );
#elif defined(TARGET_SDL)
    Rect pos;
    pos.x = x;
    pos.y = y;
    Rect frame = imageTable->frameRect;
    frame.x += frameIndex * imageTable->frameRect.h;
    SDL_BlitSurface( imageTable->imageTable, &frame, sys->sdl_surface, &pos );
#endif
}

//                                                                                         -----------
//                                                                                         - TEXTURE -
//----------------------------------------------------------------------------------------------------
struct Texture Pan_TryLoadTexture( struct SYSTEM* sys, const char* path )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    Pan_LogValueString( sys, __FILE__, __func__, __LINE__, MINIMAL, "Path", path );
    struct Texture texture;
    texture.texture = NULL;
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    const char* err;
    texture.texture = sys->playdate->graphics->loadBitmap( path, &err );
    if ( texture.texture == NULL )
    {
        sys->playdate->system->error( "%s:%i loadBitmap %s: %s", __FILE__, __LINE__, path, err );
    }
#elif defined(TARGET_SDL)
    if ( sys->hardwareRendering == false )
    {
        Pan_LogError( sys, __FILE__, __func__, __LINE__, MINIMAL, "Calling Pan_TryLoadTexture; not available when hardware rendering is off! Use Image instead!", ":(" );
//      return;
    }

    SDL_Surface* load = IMG_Load( path );
    if ( load == NULL )
    {
        Pan_LogError( sys, __FILE__, __func__, __LINE__, MINIMAL, "IMG_Load", IMG_GetError() );
        Pan_LogValueString( sys, __FILE__, __func__, __LINE__, MINIMAL, "Path", path );
    }
    else
    {
        texture.texture = SDL_CreateTextureFromSurface( sys->sdl_renderer, load );
        if ( texture.texture == NULL )
        {
            Pan_LogError( sys, __FILE__, __func__, __LINE__, MINIMAL, "SDL_CreateTextureFromSurface", SDL_GetError() );
        }
        SDL_FreeSurface( load );
    }
#endif

    return texture;
}

void Pan_FreeTexture( struct SYSTEM* sys, struct Texture* texture )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
    SDL_DestroyTexture( texture->texture );
#endif
}

void Pan_DrawTexture( struct SYSTEM* sys, struct Texture* texture, struct Rect* frameClip, struct Rect* destination )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    sys->playdate->graphics->drawBitmap( texture->texture, destination->x, destination->y, 0 );
#elif defined(TARGET_SDL)
    SDL_RenderCopy( sys->sdl_renderer, texture->texture, frameClip, destination );
#endif
}


//                                                                                   -----------------
//                                                                                   - TEXTURE TABLE -
//----------------------------------------------------------------------------------------------------
struct TextureTable Pan_TryLoadTextureTable( struct SYSTEM* sys, const char* path, int frameWidth, int frameHeight, const char* ext )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    Pan_LogValueString( sys, __FILE__, __func__, __LINE__, MINIMAL, "Path", path );
    struct TextureTable textureTable;
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    const char* err;
    textureTable.textureTable = sys->playdate->graphics->loadBitmapTable( path, &err );
    if ( textureTable.textureTable == NULL )
    {
        sys->playdate->system->error( "%s:%i loadBitmapTable %s: %s", __FILE__, __LINE__, path, err );
    }
#elif defined(TARGET_SDL)
    if ( sys->hardwareRendering == false )
    {
        Pan_LogError( sys, __FILE__, __func__, __LINE__, MINIMAL, "Calling Pan_TryLoadTexture; not available when hardware rendering is off! Use Image instead!", ":(" );
//      return;
    }

    char fullpath[100];
    sprintf( fullpath, "%s-table-%d-%d.%s", path, frameWidth, frameHeight, ext );
    textureTable.textureTable = Pan_TryLoadTexture( sys, fullpath ).texture;
#endif

    textureTable.frameRect.x = 0;
    textureTable.frameRect.y = 0;
    textureTable.frameRect.w = frameWidth;
    textureTable.frameRect.h = frameHeight;

    return textureTable;
}

void Pan_FreeTextureTable( struct SYSTEM* sys, struct TextureTable* textureTable )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
    SDL_DestroyTexture( textureTable->textureTable );
#endif
}

void Pan_DrawTextureTable( struct SYSTEM* sys, struct TextureTable* textureTable, struct Rect* destination, int frameIndex )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    LCDBitmap* currentFrame = sys->playdate->graphics->getTableBitmap( textureTable->textureTable, frameIndex );
    sys->playdate->graphics->drawBitmap( currentFrame, destination->x, destination->y, 0 );
#elif defined(TARGET_SDL)
    Rect frame = textureTable->frameRect;
    frame.x += frameIndex * textureTable->frameRect.h;
//  SDL_BlitSurface( imageTable->imageTable, &frame, sys->sdl_surface, &pos );
    SDL_RenderCopy( sys->sdl_renderer, textureTable->textureTable, &frame, destination );
#endif
}

//                                                                                           ---------
//                                                                                           - SOUND -
//----------------------------------------------------------------------------------------------------

struct Sound Pan_TryLoadSound( struct SYSTEM* sys, const char* path )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    Pan_LogValueString( sys, __FILE__, __func__, __LINE__, MINIMAL, "Path", path );
    struct Sound sound;
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    sound.sound = sys->playdate->sound->sample->load( path );
    if ( sound.sound == NULL )
    {
        sys->playdate->system->error( "%s:%i ERROR: sample->load:  %s", __FILE__, __LINE__, path );
        printf( "%s:%i ERROR: sample->load:  %s", __FILE__, __LINE__, path );
    }
#elif defined(TARGET_SDL)
    sound.sound = Mix_LoadWAV( path );
    if ( sound.sound == NULL )
    {
        Pan_LogError( sys, __FILE__, __func__, __LINE__, MINIMAL, "Mix_LoadWAV", Mix_GetError() );
        Pan_LogValueString( sys, __FILE__, __func__, __LINE__, MINIMAL, "Path",  path );
    }
#endif
    Pan_SoundVolume( &sound, sys->soundVolume.current );
    return sound;
}

void Pan_FreeSound( struct SYSTEM* sys, struct Sound* sound )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    sys->playdate->sound->sample->freeSample( sound->sound );
#elif defined(TARGET_SDL)
    Mix_FreeChunk( sound->sound );
#endif
}

void Pan_PlaySound( struct SYSTEM* sys, struct Sound* sound )
{
//  Pan_LogFunctionFile( "\nFUNCTION BEGIN %s (%s) \n", __func__, __FILE__ );
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    sys->playdate->sound->sampleplayer->setSample( sys->pd_soundplayer, sound->sound );
    sys->playdate->sound->sampleplayer->play( sys->pd_soundplayer, 1, 1.0 );
#elif defined(TARGET_SDL)
    Mix_PlayChannel( -1, sound->sound, 0 );
#endif
}


/**
   @param      sound           Pointer to the sound to adjust the volume of
   @param      volume          Volume between 0 (silent) and MIX_MAX_VOLUME (128)
   Sets the volume of the sound clip
*/
void Pan_SoundVolume( struct Sound* sound, int volume )
{
    //  Pan_LogFunctionFile( "\nFUNCTION BEGIN %s (%s) \n", __func__, __FILE__ );
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
    Mix_VolumeChunk( sound->sound, volume );
#endif
}

void Pan_AllSoundVolume( struct SYSTEM* sys, int volume )
{
    sys->soundVolume.current = volume;

    if ( sys->soundVolume.current > sys->soundVolume.maximum )
    {
        sys->soundVolume.current = sys->soundVolume.maximum;
    }
    else if ( sys->soundVolume.current < 0 )
    {
        sys->soundVolume.current = 0;
    }

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
    Mix_VolumeMusic( sys->soundVolume.current );
#endif

    Pan_LogValueInt( sys, __FILE__, __func__, __LINE__, MINIMAL, "Sound volume", sys->musicVolume.current );
}

//                                                                                           ---------
//                                                                                           - MUSIC -
//----------------------------------------------------------------------------------------------------
struct Music Pan_TryLoadMusic( struct SYSTEM* sys, const char* path, const char* ext )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    Pan_LogValueString( sys, __FILE__, __func__, __LINE__, MINIMAL, "Path", path );
    struct Music music;
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    int result = sys->playdate->sound->fileplayer->loadIntoPlayer( sys->pd_fileplayer, path );
    if ( result == 0 )
    {
        printf( "%s:%i ERROR: fileplayer->loadIntoPlayer: Can't find file:  %s", __FILE__, __LINE__, path );
    }
#elif defined(TARGET_SDL)
    char fullpath[100];
    sprintf( fullpath, "%s.%s", path, ext );
    music.music = Mix_LoadMUS( fullpath );
    if ( music.music == NULL )
    {
        printf( "%s:%i ERROR: Mix_LoadMUS:  %s: %s", __FILE__, __LINE__, path, Mix_GetError() );
    }
#endif
    return music;
}

void Pan_FreeMusic( struct SYSTEM* sys, struct Music* music )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
    Mix_FreeMusic( music->music );
#endif
}

void Pan_PlayMusic( struct SYSTEM* sys, struct Music* music )
{
//  Pan_LogFunctionFile( "\nFUNCTION BEGIN %s (%s) \n", __func__, __FILE__ );
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    sys->playdate->sound->fileplayer->play( sys->pd_fileplayer, 0 );
#elif defined(TARGET_SDL)
    if ( Mix_PlayingMusic() == 0 )
    {
        Mix_PlayMusic( music->music, -1 );
    }
#endif
}

void Pan_StopMusic( struct SYSTEM* sys )
{
//  Pan_LogFunctionFile( "\nFUNCTION BEGIN %s (%s) \n", __func__, __FILE__ );
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
    Mix_HaltMusic();
#endif
}

void Pan_PauseMusic( struct SYSTEM* sys )
{
//  Pan_LogFunctionFile( "\nFUNCTION BEGIN %s (%s) \n", __func__, __FILE__ );
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    sys->playdate->sound->fileplayer->pause( sys->pd_fileplayer );
#elif defined(TARGET_SDL)
    Mix_PauseMusic();
#endif
}

void Pan_ResumeMusic( struct SYSTEM* sys )
{
//  Pan_LogFunctionFile( "\nFUNCTION BEGIN %s (%s) \n", __func__, __FILE__ );
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    sys->playdate->sound->fileplayer->play( sys->pd_fileplayer, 0 );
#elif defined(TARGET_SDL)
    Mix_ResumeMusic();
#endif
}

void Pan_ToggleMusic( struct SYSTEM* sys )
{
//  Pan_LogFunctionFile( "\nFUNCTION BEGIN %s (%s) \n", __func__, __FILE__ );
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    if (  sys->playdate->sound->fileplayer->isPlaying( sys->pd_fileplayer ) == 1 )
    {
        Pan_PauseMusic( sys );
    }
    else
    {
        Pan_ResumeMusic( sys );
    }
#elif defined(TARGET_SDL)
    if ( Mix_PausedMusic() == 1 )
    {
        Pan_ResumeMusic( sys );
    }
    else
    {
        Pan_PauseMusic( sys );
    }
#endif
}

/**
   @param      volume          Volume between 0 (silent) and MIX_MAX_VOLUME (128)
   Sets the volume of the music channel
*/
void Pan_MusicVolume( struct SYSTEM* sys, int volume )
{
    sys->musicVolume.current = volume;

    if ( sys->musicVolume.current > sys->musicVolume.maximum )
    {
        sys->musicVolume.current = sys->musicVolume.maximum;
    }
    else if ( sys->musicVolume.current < 0 )
    {
        sys->musicVolume.current = 0;
    }

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
    Mix_VolumeMusic( sys->musicVolume.current );
#endif

    Pan_LogValueInt( sys, __FILE__, __func__, __LINE__, MINIMAL, "Music volume", sys->musicVolume.current );
}


struct MenuText Pan_SetupMenuText( struct SYSTEM* sys, char text[25], int x, int y, struct Rect basePosition, struct Font* font )
{
    struct MenuText menuText;
    menuText.font = font;
    strcpy( menuText.text, text );
    menuText.position.x = x;
    menuText.position.y = y;
//  if ( basePosition != NULL )
//  {
    menuText.position.x += basePosition.x;
    menuText.position.y += basePosition.y;
//  }
    menuText.position.w = -1;
    menuText.position.h = -1;
    return menuText;
}

void Pan_DrawMenuText( struct SYSTEM* sys, struct MenuText* menuText )
{
    Pan_DrawText( sys, menuText->font, menuText->text, &menuText->position );
}

struct MenuIcon Pan_SetupMenuIcon( struct SYSTEM* sys, struct TextureTable* textureTable, int index, int x, int y, int width, int height, struct Rect* basePosition )
{
    struct MenuIcon menuIcon;
    menuIcon.icon = textureTable;
    menuIcon.iconIndex = index;
    menuIcon.position.x = x;
    menuIcon.position.y = y;
    if ( basePosition != NULL )
    {
        menuIcon.position.x += basePosition->x;
        menuIcon.position.y += basePosition->y;
    }
    menuIcon.position.w = width;
    menuIcon.position.h = height;
    menuIcon.icon->frameRect.w = width;
    menuIcon.icon->frameRect.h = height;
    return menuIcon;
}

void Pan_DrawMenuIcon( struct SYSTEM* sys, struct MenuIcon* menuIcon )
{
    if ( menuIcon->icon != NULL )
    {
        Pan_DrawTextureTable( sys, menuIcon->icon, &menuIcon->position, menuIcon->iconIndex );
    }
}

struct MenuButton Pan_SetupMenuButton( struct SYSTEM* sys, struct Texture* texture, int x, int y, int width, int height )
{
    struct MenuButton menuButton;
    menuButton.menuIcon.icon = NULL;
    menuButton.menuText.font = NULL;
    menuButton.background = texture;
    menuButton.position.x = x;
    menuButton.position.y = y;
    menuButton.position.w = width;
    menuButton.position.h = height;
    return menuButton;
}

void Pan_DrawMenuButton( struct SYSTEM* sys, struct MenuButton* menuButton )
{
    if ( menuButton->background != NULL )
    {
        Pan_DrawTexture( sys, menuButton->background, NULL, &menuButton->position );
    }

    Pan_DrawMenuIcon( sys, &menuButton->menuIcon );
    Pan_DrawMenuText( sys, &menuButton->menuText );
}

void Pan_DrawRectangle( struct SYSTEM* sys, struct Rect rectangle, int shade )
{
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
/*
  typedef enum
  {
  kColorBlack,
  kColorWhite,
  kColorClear,
  kColorXOR
  } LCDSolidColor;
*/
#elif defined(TARGET_SDL)
    if ( shade == 0 || shade == 2 )
    {
        SDL_SetRenderDrawColor( sys->sdl_renderer, 0, 0, 0, 255 );
    }
    else if ( shade == 1 || shade == 3 )
    {
        SDL_SetRenderDrawColor( sys->sdl_renderer, 255, 255, 255, 255 );
    }
    SDL_RenderDrawRect( sys->sdl_renderer, &rectangle );
#endif
}

void Pan_FillRectangle( struct SYSTEM* sys, struct Rect rectangle, int shade )
{
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
    if ( shade == 0 || shade == 2 )
    {
        SDL_SetRenderDrawColor( sys->sdl_renderer, 0, 0, 0, 255 );
    }
    else if ( shade == 1 || shade == 3 )
    {
        SDL_SetRenderDrawColor( sys->sdl_renderer, 255, 255, 255, 255 );
    }
    SDL_RenderFillRect( sys->sdl_renderer, &rectangle );
#endif
}

