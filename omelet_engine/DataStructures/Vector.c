#include "Vector.h"
#include <stddef.h>
#include <stdlib.h>

void Vector_Init( struct Vector* vec )
{
    vec->Setup      = Vector_Setup;
    vec->Teardown   = Vector_Teardown;
    vec->Resize     = Vector_Resize;
    vec->PushBack   = Vector_PushBack;
    vec->PopBack    = Vector_PopBack;
    vec->GetBack    = Vector_GetBack;
    vec->PushFront  = Vector_PushFront;
    vec->PopFront   = Vector_PopFront;
    vec->GetFront   = Vector_GetFront;
    vec->PushAt     = Vector_PushAt;
    vec->PopAt      = Vector_PopAt;
    vec->GetAt      = Vector_GetAt;
    vec->ShiftLeft  = Vector_ShiftLeft;
    vec->ShiftRight = Vector_ShiftRight;
    vec->arr        = NULL;
}

void Vector_Setup( struct Vector* vec, int size )
{
    // arr = new TYPE[size]; in C++ :P
    vec->arr = malloc( sizeof( void* ) * size );
    vec->arraySize = size;
    vec->itemCount = 0;
}

void Vector_Teardown( struct Vector* vec )
{
    free( vec->arr );
    vec->arr = NULL;
}

void Vector_Resize( struct Vector* vec, int newSize )
{
    void ** newArray;
    newArray = malloc( sizeof( void* ) * newSize );

    for ( int i = 0; i < vec->arraySize; i++ )
    {
        newArray[i] = vec->arr[i];
    }

    free( vec->arr );
    vec->arr = newArray;
    vec->arraySize = newSize;
}

void Vector_PushBack( struct Vector* vec, void* data )
{
    if ( vec->itemCount == vec->arraySize )
    {
        vec->Resize( vec, vec->arraySize + 10 );
    }

    vec->arr[ vec->itemCount ] = data;
    vec->itemCount++;
}

void Vector_PopBack( struct Vector* vec )
{
    if ( vec->itemCount == 0 ) { return; }
    vec->itemCount--;
}

void* Vector_GetBack( struct Vector* vec )
{
    if ( vec->itemCount == 0 ) { return NULL; }
    return vec->arr[ vec->itemCount-1 ];
}

void Vector_PushFront( struct Vector* vec, void* data )
{
    if ( vec->itemCount == vec->arraySize )
    {
        vec->Resize( vec, vec->arraySize + 10 );
    }

    vec->ShiftRight( vec, 0 );
    vec->arr[ 0 ] = data;
    vec->itemCount++;
}

void Vector_PopFront( struct Vector* vec )
{
    if ( vec->itemCount == 0 ) { return; }
    vec->ShiftLeft( vec, 0 );
    vec->itemCount--;
}

void* Vector_GetFront( struct Vector* vec )
{
    if ( vec->itemCount == 0 ) { return NULL; }
    return vec->arr[ 0 ];
}

void Vector_PushAt( struct Vector* vec, int index, void* data )
{
    if      ( index < 0 || index > vec->itemCount ) { return; }
    else if ( index == 0 )              { vec->PushFront( vec, data ); }
    else if ( index == vec->itemCount ) { vec->PushBack( vec, data ); }
    else if ( vec->itemCount == vec->arraySize ) { vec->Resize( vec, vec->arraySize+10 ); }
    else
    {
        vec->ShiftRight( vec, index );
        vec->arr[index] = data;
        vec->itemCount++;
    }
}

void Vector_PopAt( struct Vector* vec, int index )
{
    if ( index < 0 || index >= vec->itemCount ) { return; }
    if ( vec->itemCount == 0 ) { return; }
    vec->ShiftLeft( vec, index );
    vec->itemCount--;
}

void* Vector_GetAt( struct Vector* vec, int index )
{
    if ( vec->itemCount == 0 ) { return NULL; }
    if ( index < 0 || index >= vec->itemCount ) { return NULL; }
    return vec->arr[index];
}

void Vector_ShiftLeft( struct Vector* vec, int index )
{
    if ( vec->itemCount == 0 ) { return; }
    for ( int i = index; i < vec->itemCount - 1; i++ )
    {
        vec->arr[i] = vec->arr[i+1];
    }
}

void Vector_ShiftRight( struct Vector* vec, int index )
{
    if ( vec->itemCount == 0 ) { return; }
    if ( vec->itemCount == vec->arraySize )
    {
        vec->Resize( vec, vec->arraySize + 10 );
    }

    for ( int i = vec->itemCount; i > index; i-- )
    {
        vec->arr[i] = vec->arr[i-1];
    }
}

void Vector_Test()
{
    char testName[100];
    char result[8];
    char* pass = "PASS";
    char* fail = "FAIL";

    { strcpy( testName, "Check array initialization" );
        struct Vector v;
        Vector_Init( &v );
        v.Setup( &v, 5 );

        if ( v.arr != NULL &&
             v.arraySize == 5 &&
             v.itemCount == 0
            )
        { strcpy( result, pass ); }
        else { strcpy( result, fail ); }

        printf( "[%s] %s \n", result, testName );
        if ( strcmp( result, fail ) == 0 )
        {
            printf( "* v.arr         expected: not NULL, actual: %p \n", v.arr );
            printf( "* v.arraySize   expected: 5,        actual: %d \n", v.arraySize );
            printf( "* v.itemCount   expected: 0,        actual: %d \n", v.itemCount );
        }

        v.Teardown( &v );
    }
    { strcpy( testName, "Call Teardown, check values" );
        struct Vector v;
        Vector_Init( &v );
        v.Setup( &v, 5 );



        v.Teardown( &v );
    }

    { strcpy( testName, "Call PushBack, check items in arr" );
        struct Vector v;
        Vector_Init( &v );
        v.Setup( &v, 5 );

        v.PushBack( &v, 2 );
        v.PushBack( &v, 4 );
        v.PushBack( &v, 6 );
        v.PushBack( &v, 8 );

        if ( v.arr[0] == 2 &&
             v.arr[1] == 4 &&
             v.arr[2] == 6 &&
             v.arr[3] == 8 &&
             v.itemCount == 4
            )
        { strcpy( result, pass ); }
        else { strcpy( result, fail ); }

        printf( "[%s] %s \n", result, testName );
        if ( strcmp( result, fail ) == 0 )
        {
            printf( "* v.arr[0]      expected: 2,     actual: %d \n", v.arr[0] );
            printf( "* v.arr[1]      expected: 4,     actual: %d \n", v.arr[1] );
            printf( "* v.arr[2]      expected: 6,     actual: %d \n", v.arr[2] );
            printf( "* v.arr[3]      expected: 8,     actual: %d \n", v.arr[3] );
            printf( "* v.itemCount   expected: 4,     actual: %d \n", v.itemCount );
        }

        v.Teardown( &v );
    }
    { strcpy( testName, "Call Resize, check array" );
        struct Vector v;
        Vector_Init( &v );
        v.Setup( &v, 5 );
        v.arr[0] = 3;
        v.arr[1] = 4;
        v.arr[2] = 5;
        v.arr[3] = 6;
        v.arr[4] = 7;

        v.itemCount = 5;

        v.Resize( &v, 10 );

        if ( v.arr[0] == 3 &&
             v.arr[1] == 4 &&
             v.arr[2] == 5 &&
             v.arr[3] == 6 &&
             v.arr[4] == 7 &&
             v.itemCount == 5 &&
             v.arraySize == 10
            )
        { strcpy( result, pass ); }
        else { strcpy( result, fail ); }

        printf( "[%s] %s \n", result, testName );
        if ( strcmp( result, fail ) == 0 )
        {
            printf( "* v.arr[0]      expected: 3,     actual: %d \n", v.arr[0] );
            printf( "* v.arr[1]      expected: 4,     actual: %d \n", v.arr[1] );
            printf( "* v.arr[2]      expected: 5,     actual: %d \n", v.arr[2] );
            printf( "* v.arr[3]      expected: 6,     actual: %d \n", v.arr[3] );
            printf( "* v.arr[4]      expected: 7,     actual: %d \n", v.arr[4] );
            printf( "* v.itemCount   expected: 5,     actual: %d \n", v.itemCount );
            printf( "* v.arraySize   expected: 10,    actual: %d \n", v.arraySize );
        }

        v.Teardown( &v );
    }
    { strcpy( testName, "Call ShiftLeft, check items in arr" );
        struct Vector v;
        Vector_Init( &v );
        v.Setup( &v, 5 );

        v.arr[0] = 3;
        v.arr[1] = 4;
        v.arr[2] = 5;
        v.arr[3] = 6;
        v.itemCount = 4;

        v.ShiftLeft( &v, 1 );

        if ( v.arr[0] == 3 &&
             v.arr[1] == 5 &&
             v.arr[2] == 6 &&
             v.itemCount == 4 // This function shouldn't update itemCount.
            )
        { strcpy( result, pass ); }
        else { strcpy( result, fail ); }

        printf( "[%s] %s \n", result, testName );
        if ( strcmp( result, fail ) == 0 )
        {
            printf( "* v.arr[0]      expected: 3,     actual: %d \n", v.arr[0] );
            printf( "* v.arr[1]      expected: 5,     actual: %d \n", v.arr[1] );
            printf( "* v.arr[2]      expected: 6,     actual: %d \n", v.arr[2] );
            printf( "* v.itemCount   expected: 4,     actual: %d \n", v.itemCount );
        }

        v.Teardown( &v );
    }
    { strcpy( testName, "Call ShiftRight, check items in arr" );
        struct Vector v;
        Vector_Init( &v );
        v.Setup( &v, 5 );

        v.arr[0] = 3;
        v.arr[1] = 4;
        v.arr[2] = 5;
        v.arr[3] = 6;
        v.itemCount = 4;

        v.ShiftRight( &v, 1 );

        if ( v.arr[0] == 3 &&
             v.arr[2] == 4 &&
             v.arr[3] == 5 &&
             v.arr[4] == 6 &&
             v.itemCount == 4 // This function shouldn't update itemCount.
            )
        { strcpy( result, pass ); }
        else { strcpy( result, fail ); }

        printf( "[%s] %s \n", result, testName );
        if ( strcmp( result, fail ) == 0 )
        {
            printf( "* v.arr[0]      expected: 3,     actual: %d \n", v.arr[0] );
            printf( "* v.arr[2]      expected: 4,     actual: %d \n", v.arr[2] );
            printf( "* v.arr[3]      expected: 5,     actual: %d \n", v.arr[3] );
            printf( "* v.arr[4]      expected: 6,     actual: %d \n", v.arr[4] );
            printf( "* v.itemCount   expected: 4,     actual: %d \n", v.itemCount );
        }

        v.Teardown( &v );
    }
    { strcpy( testName, "Call ShiftRight, Resize called? check items in arr" );
        struct Vector v;
        Vector_Init( &v );
        v.Setup( &v, 5 );

        v.arr[0] = 11;
        v.arr[1] = 22;
        v.arr[2] = 33;
        v.arr[3] = 44;
        v.arr[4] = 55;
        v.itemCount = 5;

        v.ShiftRight( &v, 2 );

        if ( v.arr[0] == 11 &&
             v.arr[1] == 22 &&
             v.arr[3] == 33 &&
             v.arr[4] == 44 &&
             v.arr[5] == 55 &&
             v.itemCount == 5 && // This function shouldn't update itemCount.
             v.arraySize > 5
            )
        { strcpy( result, pass ); }
        else { strcpy( result, fail ); }

        printf( "[%s] %s \n", result, testName );
        if ( strcmp( result, fail ) == 0 )
        {
            printf( "* v.arr[0]      expected: 11,     actual: %d \n", v.arr[0] );
            printf( "* v.arr[1]      expected: 22,     actual: %d \n", v.arr[1] );
            printf( "* v.arr[3]      expected: 33,     actual: %d \n", v.arr[3] );
            printf( "* v.arr[4]      expected: 44,     actual: %d \n", v.arr[4] );
            printf( "* v.arr[5]      expected: 55,     actual: %d \n", v.arr[5] );
            printf( "* v.itemCount   expected: 5,     actual: %d \n", v.itemCount );
            printf( "* v.arraySize   expected: >5,    actual: %d \n", v.arraySize );
        }

        v.Teardown( &v );
    }
    { strcpy( testName, "Call PushBack, Resize called? Check values" );
        struct Vector v;
        Vector_Init( &v );
        v.Setup( &v, 5 );

        v.PushBack( &v, 2 );
        v.PushBack( &v, 4 );
        v.PushBack( &v, 6 );
        v.PushBack( &v, 8 );
        v.PushBack( &v, 1 );
        v.PushBack( &v, 3 );
        v.PushBack( &v, 5 );

        if ( v.arr[0] == 2 &&
             v.arr[1] == 4 &&
             v.arr[2] == 6 &&
             v.arr[3] == 8 &&
             v.arr[4] == 1 &&
             v.arr[5] == 3 &&
             v.arr[6] == 5 &&
             v.itemCount == 7 &&
             v.arraySize > 5
            )
        { strcpy( result, pass ); }
        else { strcpy( result, fail ); }

        printf( "[%s] %s \n", result, testName );
        if ( strcmp( result, fail ) == 0 )
        {
            printf( "* v.arr[0]      expected: 2,     actual: %d \n", v.arr[0] );
            printf( "* v.arr[1]      expected: 4,     actual: %d \n", v.arr[1] );
            printf( "* v.arr[2]      expected: 6,     actual: %d \n", v.arr[2] );
            printf( "* v.arr[3]      expected: 8,     actual: %d \n", v.arr[3] );
            printf( "* v.arr[4]      expected: 1,     actual: %d \n", v.arr[4] );
            printf( "* v.arr[5]      expected: 2,     actual: %d \n", v.arr[5] );
            printf( "* v.arr[6]      expected: 3,     actual: %d \n", v.arr[6] );
            printf( "* v.itemCount   expected: 7,     actual: %d \n", v.itemCount );
            printf( "* v.arraySize   expected: >5,    actual: %d \n", v.arraySize );
        }

        v.Teardown( &v );
    }
    { strcpy( testName, "Call PushFront, Resize called? Check values" );
        struct Vector v;
        Vector_Init( &v );
        v.Setup( &v, 5 );

        v.PushFront( &v, 2 );
        v.PushFront( &v, 4 );
        v.PushFront( &v, 6 );
        v.PushFront( &v, 8 );
        v.PushFront( &v, 1 );
        v.PushFront( &v, 3 );
        v.PushFront( &v, 5 );

        if ( v.arr[0] == 5 &&
             v.arr[1] == 3 &&
             v.arr[2] == 1 &&
             v.arr[3] == 8 &&
             v.arr[4] == 6 &&
             v.arr[5] == 4 &&
             v.arr[6] == 2 &&
             v.itemCount == 7 &&
             v.arraySize > 5
            )
        { strcpy( result, pass ); }
        else { strcpy( result, fail ); }

        printf( "[%s] %s \n", result, testName );
        if ( strcmp( result, fail ) == 0 )
        {
            printf( "* v.arr[0]      expected: 5,     actual: %d \n", v.arr[0] );
            printf( "* v.arr[1]      expected: 3,     actual: %d \n", v.arr[1] );
            printf( "* v.arr[2]      expected: 1,     actual: %d \n", v.arr[2] );
            printf( "* v.arr[3]      expected: 8,     actual: %d \n", v.arr[3] );
            printf( "* v.arr[4]      expected: 6,     actual: %d \n", v.arr[4] );
            printf( "* v.arr[5]      expected: 4,     actual: %d \n", v.arr[5] );
            printf( "* v.arr[6]      expected: 2,     actual: %d \n", v.arr[6] );
            printf( "* v.itemCount   expected: 7,     actual: %d \n", v.itemCount );
            printf( "* v.arraySize   expected: >5,    actual: %d \n", v.arraySize );
        }

        v.Teardown( &v );
    }
    { strcpy( testName, "Call PushAt, Resize called? Check values" );
        struct Vector v;
        Vector_Init( &v );
        v.Setup( &v, 5 );

        v.arr[0] = 10;
        v.arr[1] = 11;
        v.arr[2] = 12;
        v.arr[3] = 13;
        v.arr[4] = 14;
        v.itemCount = 5;

        v.PushAt( &v, 0, 1 );
        v.PushAt( &v, 3, 1 );
        v.PushAt( &v, 6, 1 );

        if ( v.arr[0] == 1 &&
             v.arr[1] == 10 &&
             v.arr[2] == 11 &&
             v.arr[3] == 1 &&
             v.arr[4] == 12 &&
             v.arr[5] == 13 &&
             v.arr[6] == 1 &&
             v.itemCount == 8 &&
             v.arraySize > 5
            )
        { strcpy( result, pass ); }
        else { strcpy( result, fail ); }

        printf( "[%s] %s \n", result, testName );
        if ( strcmp( result, fail ) == 0 )
        {
            printf( "* v.arr[0]      expected: 1,     actual: %d \n", v.arr[0] );
            printf( "* v.arr[1]      expected: 10,    actual: %d \n", v.arr[1] );
            printf( "* v.arr[2]      expected: 11,    actual: %d \n", v.arr[2] );
            printf( "* v.arr[3]      expected: 1,     actual: %d \n", v.arr[3] );
            printf( "* v.arr[4]      expected: 12,    actual: %d \n", v.arr[4] );
            printf( "* v.arr[5]      expected: 13,    actual: %d \n", v.arr[5] );
            printf( "* v.arr[6]      expected: 1,     actual: %d \n", v.arr[6] );
            printf( "* v.itemCount   expected: 8,     actual: %d \n", v.itemCount );
            printf( "* v.arraySize   expected: >5,    actual: %d \n", v.arraySize );
        }

        v.Teardown( &v );
    }
    { strcpy( testName, "Call PushAt with invalid index; no change" );
        struct Vector v;
        Vector_Init( &v );
        v.Setup( &v, 5 );

        v.arr[0] = 10;
        v.arr[1] = 11;
        v.arr[2] = 12;
        v.itemCount = 3;

        v.PushAt( &v, 100, 1 );
        v.PushAt( &v, -1, 1 );

        if ( v.arr[0] == 10 &&
             v.arr[1] == 11 &&
             v.arr[2] == 12 &&
             v.itemCount == 3
            )
        { strcpy( result, pass ); }
        else { strcpy( result, fail ); }

        printf( "[%s] %s \n", result, testName );
        if ( strcmp( result, fail ) == 0 )
        {
            printf( "* v.arr[0]      expected: 10,     actual: %d \n", v.arr[0] );
            printf( "* v.arr[1]      expected: 11,     actual: %d \n", v.arr[1] );
            printf( "* v.arr[2]      expected: 12,     actual: %d \n", v.arr[2] );
            printf( "* v.itemCount   expected: 3,      actual: %d \n", v.itemCount );
        }

        v.Teardown( &v );
    }
    { strcpy( testName, "Call PopBack, check values" );
        struct Vector v;
        Vector_Init( &v );
        v.Setup( &v, 5 );

        v.arr[0] = 11;
        v.arr[1] = 22;
        v.arr[2] = 33;
        v.arr[3] = 44;
        v.arr[4] = 55;
        v.itemCount = 5;

        v.PopBack( &v );

        if ( v.itemCount == 4 &&
             v.arr[0] == 11 &&
             v.arr[1] == 22 &&
             v.arr[2] == 33 &&
             v.arr[3] == 44 )
        { strcpy( result, pass ); }
        else { strcpy( result, fail ); }

        printf( "[%s] %s \n", result, testName );
        if ( strcmp( result, fail ) == 0 )
        {
            printf( "* v.arr[0]      expected: 11,     actual: %d \n", v.arr[0] );
            printf( "* v.arr[1]      expected: 22,     actual: %d \n", v.arr[1] );
            printf( "* v.arr[2]      expected: 33,     actual: %d \n", v.arr[2] );
            printf( "* v.arr[2]      expected: 44,     actual: %d \n", v.arr[2] );
            printf( "* v.itemCount   expected: 4,      actual: %d \n", v.itemCount );
        }

        v.Teardown( &v );
    }

    { strcpy( testName, "Call PopFront, check values" );
        struct Vector v;
        Vector_Init( &v );
        v.Setup( &v, 5 );

        v.arr[0] = 11;
        v.arr[1] = 22;
        v.arr[2] = 33;
        v.arr[3] = 44;
        v.arr[4] = 55;
        v.itemCount = 5;

        v.PopFront( &v );

        if ( v.itemCount == 4 &&
             v.arr[0] == 22 &&
             v.arr[1] == 33 &&
             v.arr[2] == 44 &&
             v.arr[3] == 55 )
        { strcpy( result, pass ); }
        else { strcpy( result, fail ); }

        printf( "[%s] %s \n", result, testName );
        if ( strcmp( result, fail ) == 0 )
        {
            printf( "* v.arr[0]      expected: 22,     actual: %d \n", v.arr[0] );
            printf( "* v.arr[1]      expected: 33,     actual: %d \n", v.arr[1] );
            printf( "* v.arr[2]      expected: 44,     actual: %d \n", v.arr[2] );
            printf( "* v.arr[2]      expected: 55,     actual: %d \n", v.arr[2] );
            printf( "* v.itemCount   expected: 4,      actual: %d \n", v.itemCount );
        }

        v.Teardown( &v );
    }

    { strcpy( testName, "Call PopAt, check values" );
        struct Vector v;
        Vector_Init( &v );
        v.Setup( &v, 5 );

        v.arr[0] = 11;
        v.arr[1] = 22;
        v.arr[2] = 33;
        v.arr[3] = 44;
        v.arr[4] = 55;
        v.itemCount = 5;

        v.PopAt( &v, 2 );

        if ( v.itemCount == 4 &&
             v.arr[0] == 11 &&
             v.arr[1] == 22 &&
             v.arr[2] == 44 &&
             v.arr[3] == 55 )
        { strcpy( result, pass ); }
        else { strcpy( result, fail ); }

        printf( "[%s] %s \n", result, testName );
        if ( strcmp( result, fail ) == 0 )
        {
            printf( "* v.arr[0]      expected: 11,     actual: %d \n", v.arr[0] );
            printf( "* v.arr[1]      expected: 22,     actual: %d \n", v.arr[1] );
            printf( "* v.arr[2]      expected: 44,     actual: %d \n", v.arr[2] );
            printf( "* v.arr[2]      expected: 55,     actual: %d \n", v.arr[2] );
            printf( "* v.itemCount   expected: 4,      actual: %d \n", v.itemCount );
        }

        v.Teardown( &v );
    }
    { strcpy( testName, "Call GetBack, check return" );
        struct Vector v;
        Vector_Init( &v );
        v.Setup( &v, 5 );

        v.arr[0] = 11;
        v.arr[1] = 22;
        v.arr[2] = 33;
        v.arr[3] = 44;
        v.arr[4] = 55;
        v.itemCount = 5;

        int expected = 55;
        int actual = v.GetBack( &v );

        if ( actual == expected ) { strcpy( result, pass ); }
        else { strcpy( result, fail ); }

        printf( "[%s] %s \n", result, testName );
        if ( strcmp( result, fail ) == 0 )
        {
            printf( "* v.GetBack()      expected: %d,     actual: %d \n", expected, actual );
        }

        v.Teardown( &v );
    }
    { strcpy( testName, "Call GetBack, check return" );
        struct Vector v;
        Vector_Init( &v );
        v.Setup( &v, 5 );

        v.arr[0] = 11;
        v.arr[1] = 22;
        v.arr[2] = 33;
        v.itemCount = 3;

        int expected = 33;
        int actual = v.GetBack( &v );

        if ( actual == expected ) { strcpy( result, pass ); }
        else { strcpy( result, fail ); }

        printf( "[%s] %s \n", result, testName );
        if ( strcmp( result, fail ) == 0 )
        {
            printf( "* v.GetBack()      expected: %d,     actual: %d \n", expected, actual );
        }

        v.Teardown( &v );
    }
    { strcpy( testName, "Call GetBack on empty vector" );
        struct Vector v;
        Vector_Init( &v );
        v.Setup( &v, 5 );

        void* expected = NULL;
        void* actual = v.GetBack( &v );

        if ( actual == expected ) { strcpy( result, pass ); }
        else { strcpy( result, fail ); }

        printf( "[%s] %s \n", result, testName );
        if ( strcmp( result, fail ) == 0 )
        {
            printf( "* v.GetBack()      expected: %p,     actual: %p \n", expected, actual );
        }

        v.Teardown( &v );
    }
    { strcpy( testName, "Call GetFront, check return" );
        struct Vector v;
        Vector_Init( &v );
        v.Setup( &v, 5 );

        v.arr[0] = 11;
        v.arr[1] = 22;
        v.arr[2] = 33;
        v.arr[3] = 44;
        v.arr[4] = 55;
        v.itemCount = 5;

        int expected = 11;
        int actual = v.GetFront( &v );

        if ( actual == expected ) { strcpy( result, pass ); }
        else { strcpy( result, fail ); }

        printf( "[%s] %s \n", result, testName );
        if ( strcmp( result, fail ) == 0 )
        {
            printf( "* v.GetFront()      expected: %d,     actual: %d \n", expected, actual );
        }

        v.Teardown( &v );
    }
    { strcpy( testName, "Call GetFront, check return" );
        struct Vector v;
        Vector_Init( &v );
        v.Setup( &v, 5 );

        v.arr[0] = 22;
        v.arr[1] = 22;
        v.arr[2] = 33;
        v.itemCount = 3;

        int expected = 22;
        int actual = v.GetFront( &v );

        if ( actual == expected ) { strcpy( result, pass ); }
        else { strcpy( result, fail ); }

        printf( "[%s] %s \n", result, testName );
        if ( strcmp( result, fail ) == 0 )
        {
            printf( "* v.GetFront()      expected: %d,     actual: %d \n", expected, actual );
        }

        v.Teardown( &v );
    }
    { strcpy( testName, "Call GetFront on empty vector" );
        struct Vector v;
        Vector_Init( &v );
        v.Setup( &v, 5 );

        void* expected = NULL;
        void* actual = v.GetFront( &v );

        if ( actual == expected ) { strcpy( result, pass ); }
        else { strcpy( result, fail ); }

        printf( "[%s] %s \n", result, testName );
        if ( strcmp( result, fail ) == 0 )
        {
            printf( "* v.GetFront()      expected: %p,     actual: %p \n", expected, actual );
        }

        v.Teardown( &v );
    }
    { strcpy( testName, "Call GetAt with valid index, check return" );
        struct Vector v;
        Vector_Init( &v );
        v.Setup( &v, 5 );

        v.arr[0] = 11;
        v.arr[1] = 22;
        v.arr[2] = 33;
        v.arr[3] = 44;
        v.arr[4] = 55;
        v.itemCount = 5;

        int input = 2;
        int expected = 33;
        int actual = v.GetAt( &v, input );

        if ( actual == expected ) { strcpy( result, pass ); }
        else { strcpy( result, fail ); }

        printf( "[%s] %s \n", result, testName );
        if ( strcmp( result, fail ) == 0 )
        {
            printf( "* v.GetAt(%d)      expected: %d,     actual: %d \n", input, expected, actual );
        }

        v.Teardown( &v );
    }
    { strcpy( testName, "Call GetAt with valid index, check return" );
        struct Vector v;
        Vector_Init( &v );
        v.Setup( &v, 5 );

        v.arr[0] = 11;
        v.arr[1] = 22;
        v.arr[2] = 33;
        v.arr[3] = 44;
        v.arr[4] = 55;
        v.itemCount = 5;

        int input = 3;
        int expected = 44;
        int actual = v.GetAt( &v, input );

        if ( actual == expected ) { strcpy( result, pass ); }
        else { strcpy( result, fail ); }

        printf( "[%s] %s \n", result, testName );
        if ( strcmp( result, fail ) == 0 )
        {
            printf( "* v.GetAt(%d)      expected: %d,     actual: %d \n", input, expected, actual );
        }

        v.Teardown( &v );
    }
    { strcpy( testName, "Call GetAt with invalid index, check return" );
        struct Vector v;
        Vector_Init( &v );
        v.Setup( &v, 5 );

        v.arr[0] = 11;
        v.arr[1] = 22;
        v.arr[2] = 33;
        v.arr[3] = 44;
        v.arr[4] = 55;
        v.itemCount = 5;

        int input = 100;
        void* expected = NULL;
        void* actual = v.GetAt( &v, input );

        if ( actual == expected ) { strcpy( result, pass ); }
        else { strcpy( result, fail ); }

        printf( "[%s] %s \n", result, testName );
        if ( strcmp( result, fail ) == 0 )
        {
            printf( "* v.GetAt(%d)      expected: %p,     actual: %p \n", input, expected, actual );
        }

        v.Teardown( &v );
    }
}


