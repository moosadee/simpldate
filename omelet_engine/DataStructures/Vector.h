#ifndef _VECTOR
#define _VECTOR

struct Vector
{
  void (*Setup)( struct Vector* vec, int size );
  void (*Teardown)( struct Vector* vec );
  void (*Resize)( struct Vector* vec, int newSize );

  void (*PushBack)( struct Vector* vec, void* data );
  void (*PopBack)( struct Vector* vec );
  void* (*GetBack)( struct Vector* vec );

  void (*PushFront)( struct Vector* vec, void* data );
  void (*PopFront)( struct Vector* vec );
  void* (*GetFront)( struct Vector* vec );

  void (*PushAt)( struct Vector* vec, int index, void* data );
  void (*PopAt)( struct Vector* vec, int index );
  void* (*GetAt)( struct Vector* vec, int index );

  void (*ShiftLeft)( struct Vector* vec, int index );
  void (*ShiftRight)( struct Vector* vec, int index );

  void ** arr;
  int arraySize;
  int itemCount;
};

void Vector_Init( struct Vector* vec );

void Vector_Setup( struct Vector* vec, int size );
void Vector_Teardown( struct Vector* vec );
void Vector_Resize( struct Vector* vec, int newSize );

void Vector_PushBack( struct Vector* vec, void* data );
void Vector_PopBack( struct Vector* vec );
void* Vector_GetBack( struct Vector* vec );
void Vector_PushFront( struct Vector* vec, void* data );
void Vector_PopFront( struct Vector* vec );
void* Vector_GetFront( struct Vector* vec );
void Vector_PushAt( struct Vector* vec, int index, void* data );
void Vector_PopAt( struct Vector* vec, int index );
void* Vector_GetAt( struct Vector* vec, int index );

void Vector_ShiftLeft( struct Vector* vec, int index );
void Vector_ShiftRight( struct Vector* vec, int index );

//! Unit tests for the Vector
void Vector_Test();

#endif
