//#include <stdio.h>

#include "Structures.h"
#include "pan_system.h"
#include "../omelet_game/OmeletBase.h"

#include <emscripten.h>

// THIS DOESN'T WORK YET.

struct SYSTEM sys;
int counter;

static void mainloop( void )
{
    if ( sys.wantToQuit == true )
    {
        emscripten_cancel_main_loop();
        //Teardown( &sys );
        return;
    }

    printf( "HI! %d \n", counter );
    counter++;

    if ( counter == 10 )
    {
        sys.wantToQuit = true;
        printf( "BYE? \n" );
    }

    /*
      CycleBegin( &sys );
      HandleInput( &sys );
      Update( &sys );
      Draw( &sys );
      CycleEnd( &sys );
      * */
}

int main()
{
    printf( "int main() \n" );
    //Setup( &sys );
    counter = 0;

    if ( sys.fatalError == true )
    {
        printf( "FATAL ERROR! \n" );
        return 1;
    }

    Pan_LogText( "** Beginning game loop **", true );
    // void emscripten_set_main_loop(em_callback_func func, int fps, int simulate_infinite_loop)
    emscripten_set_main_loop(mainloop, 0, 1);
    Pan_LogText( "** Quitting now **", true );

    //Teardown( &sys );

    return 0;
}
