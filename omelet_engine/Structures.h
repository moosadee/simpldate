#ifndef _STRUCTURES
#define _STRUCTURES

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#include "pd_system.h"
#elif defined(TARGET_SDL)
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#endif

#include <stdbool.h>

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
struct Rect
{
    int x, y, w, h;
};
#elif defined(TARGET_SDL)
#define Rect SDL_Rect
#else
#define Rect SDL_Rect
#endif

enum InputState {
    INACTIVE = 0, BEGIN_PUSH = 1, HELD_DOWN = 2, BEGIN_RELEASE = 3
};

enum LogLevel {
    SILENT = 0, MINIMAL = 1, VERBOSE = 2
};

struct Counter {
    float current;
    float maximum;
    float increment;
};

struct Input {
    enum InputState buttonLeft;
    enum InputState buttonRight;
    enum InputState buttonUp;
    enum InputState buttonDown;
    enum InputState buttonActionL;
    enum InputState buttonActionR;
};

#if defined(TARGET_SDL)
struct Timer {
    Uint32 startTicks;
    Uint32 frameTicks;
};
#endif


struct Font {
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    LCDBitmapTable* table;
#elif defined(TARGET_SDL)
//  SDL_Surface* table; // software
    SDL_Texture* table;   // hardware
#endif
    struct Rect frameRect;
};

struct SYSTEM;

struct State {
    void (*Setup)( struct SYSTEM* sys );
    void (*Teardown)( struct SYSTEM* sys );
    void (*HandleInput)( struct SYSTEM* sys );
    void (*Update)( struct SYSTEM* sys );
    void (*Draw)( struct SYSTEM* sys );
    void (*CycleBegin)( struct SYSTEM* sys );
    void (*CycleEnd)( struct SYSTEM* sys );
};

//! Basic software-rendered image (SDL)
struct Image {
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    LCDBitmap* image;
#elif defined(TARGET_SDL)
    SDL_Surface* image;
#endif
};

//! Image with frames of animation
struct ImageTable {
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    LCDBitmapTable* imageTable;
#elif defined(TARGET_SDL)
    SDL_Surface* imageTable;
#endif
    struct Rect frameRect;
};

//! Hardware-rendered image (SDL)
struct Texture {
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    LCDBitmap* texture;
#elif defined(TARGET_SDL)
    SDL_Texture* texture;
#endif
};

//! Hardware-rendered image table (SDL)
struct TextureTable {
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    LCDBitmapTable* textureTable;
#elif defined(TARGET_SDL)
    SDL_Texture* textureTable;
#endif
    struct Rect frameRect;
};

struct Sound {
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    AudioSample* sound;
#elif defined(TARGET_SDL)
    Mix_Chunk* sound;
#endif
};

struct Music {
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    //AudioSample* music;
#elif defined(TARGET_SDL)
    Mix_Music* music;
#endif
};

enum Direction {
    SOUTH = 0,
    EAST = 1,
    NORTH = 2,
    WEST = 3
};

struct MenuText {
    struct Font* font;
    struct Rect position;
    char text[100];
};

struct MenuIcon {
    struct TextureTable* icon;
    int iconIndex;
    struct Rect position;
};

struct MenuButton {
    struct MenuText menuText;
    struct MenuIcon menuIcon;
    struct Texture* background;
    struct Rect position;
};

struct MapTile {
    struct Rect position;
    struct Rect frameRect;
    struct TextureTable* textureTable;
};

struct Map {
    char name[100];
    struct Rect dimensions;
    struct Rect tileDimensions;
    struct Rect cameraOffset;
    struct TextureTable textureTable;
//  struct MapTile** tiles;
    struct MapTile tiles[80][60];
};


struct SYSTEM {
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
    PlaydateAPI* playdate;
    SamplePlayer* pd_soundplayer;
    FilePlayer* pd_fileplayer;
#elif defined(TARGET_SDL)
    SDL_Window* sdl_window;
    SDL_Surface* sdl_surface;
    SDL_Renderer* sdl_renderer;
    struct Timer fpsTimer;
    struct Timer capTimer;
    bool hardwareRendering;
    int mouseX, mouseY;
    bool mouseDown;
#endif
    bool fatalError;
    bool wantToQuit;
    struct Rect renderBounds;
    struct Rect windowBounds;
    struct Rect scaleRatio;
    struct Input input;
    int ticksPerFrame;
    int fps;
    char nextState[25]; // ""
    enum LogLevel logLevel; // 0 for none, 1 for min, 2 for all
    struct Counter musicVolume;
    struct Counter soundVolume;
    struct Counter globalInterval;
    struct State currentState;

    struct {
        struct Font largeFont;
        struct Font mediumFont;
        struct Font smallFont;
        struct Texture menuBackground;
        struct Texture buttonBackground;
        struct Texture cursor;
        struct TextureTable menuIcons;
    } GLOBAL;
};

#endif
