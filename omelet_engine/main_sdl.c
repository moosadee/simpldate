//#include <stdio.h>

#include "Structures.h"
#include "pan_system.h"
#include "../omelet_game/OmeletBase.h"
#include "DataStructures/Vector.h"

int main()
{
    Vector_Test();

    struct SYSTEM sys;
    Setup( &sys );

    if ( sys.fatalError == true )
    {
        printf( "FATAL ERROR! \n" );
        return 1;
    }

    Pan_LogFunctionBeginEnd( &sys, __FILE__, __func__, __LINE__, "** Beginning game loop **" );
    while ( sys.wantToQuit == false )
    {
        CycleBegin( &sys );
        HandleInput( &sys );
        Update( &sys );
        Draw( &sys );
        CycleEnd( &sys );
    }
    Pan_LogFunctionBeginEnd( &sys, __FILE__, __func__, __LINE__, "** Quitting now **" );

    Teardown( &sys );

    printf( "Thank you for playing! :) \n" );

    return 0;
}
