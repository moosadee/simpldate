#ifndef _UTILITIES
#define _UTILITIES

#include "Structures.h"

bool BoundingBoxCollision( struct Rect rect1, struct Rect rect2 );
void SetRandomScreenPosition( struct SYSTEM* sys, struct Rect* rect );
struct Rect GetRect( int x, int y, int w, int h );
bool PointInBox( int x, int y, struct Rect box );
struct Rect GetOffsetRect( struct Rect original, struct Rect offset );

#endif
