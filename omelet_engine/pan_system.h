#ifndef _PAN_SYSTEM
#define _PAN_SYSTEM

#include <stdbool.h>

#include "Structures.h"

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#include "pd_system.h"
#elif defined(TARGET_SDL)
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#endif

void Pan_Setup( struct SYSTEM* sys, int renderWidth, int renderHeight, int windowWidth, int windowHeight, const char* windowTitle, bool hardwareRendering );
void Pan_Teardown( struct SYSTEM* sys );

void Pan_ClearScreen( struct SYSTEM* sys );
void Pan_DrawScreen( struct SYSTEM* sys );

void Pan_HandleInput( struct SYSTEM* sys );
void Pan_Update( struct SYSTEM* sys );

void Pan_CycleBegin( struct SYSTEM* sys );
void Pan_CycleEnd( struct SYSTEM* sys );

void Pan_IncrementCounter( struct Counter* counter );

void Pan_LogSetup            ( struct SYSTEM* sys, const char* file, const char* func, int line, enum LogLevel logLevel );
void Pan_LogTeardown         ( struct SYSTEM* sys, const char* file, const char* func, int line );
void Pan_LogText             ( struct SYSTEM* sys, const char* file, const char* func, int line, enum LogLevel logLevel, const char* text );
void Pan_LogValueString      ( struct SYSTEM* sys, const char* file, const char* func, int line, enum LogLevel logLevel, const char* label, const char* value );
void Pan_LogValueInt         ( struct SYSTEM* sys, const char* file, const char* func, int line, enum LogLevel logLevel, const char* label, int value );
void Pan_LogFunctionBeginEnd ( struct SYSTEM* sys, const char* file, const char* func, int line, const char* text );
void Pan_LogError            ( struct SYSTEM* sys, const char* file, const char* func, int line, enum LogLevel logLevel, const char* text, const char* error );

struct Font Pan_TryLoadFont( struct SYSTEM* sys, const char* path, int size, const char* ext );
void Pan_FreeFont( struct SYSTEM* sys, struct Font* font );
void Pan_DrawText( struct SYSTEM* sys, struct Font* font, const char* text, struct Rect* destination );

struct Image Pan_TryLoadImage( struct SYSTEM* sys, const char* path );
void Pan_FreeImage( struct SYSTEM* sys, struct Image* image );
void Pan_DrawImage( struct SYSTEM* sys, struct Image* image, int x, int y );

struct ImageTable Pan_TryLoadImageTable( struct SYSTEM* sys, const char* path, int frameWidth, int frameHeight, const char* ext );
void Pan_FreeImageTable( struct SYSTEM* sys, struct ImageTable* imageTable );
void Pan_DrawImageTable( struct SYSTEM* sys, struct ImageTable* imageTable, int x, int y, int frameIndex );

struct Texture Pan_TryLoadTexture( struct SYSTEM* sys, const char* path );
void Pan_FreeTexture( struct SYSTEM* sys, struct Texture* texture );
void Pan_DrawTexture( struct SYSTEM* sys, struct Texture* texture, struct Rect* frameClip, struct Rect* destination );

struct TextureTable Pan_TryLoadTextureTable( struct SYSTEM* sys, const char* path, int frameWidth, int frameHeight, const char* ext );
void Pan_FreeTextureTable( struct SYSTEM* sys, struct TextureTable* textureTable );
void Pan_DrawTextureTable( struct SYSTEM* sys, struct TextureTable* textureTable, struct Rect* destination, int frameIndex );

struct Sound Pan_TryLoadSound( struct SYSTEM* sys, const char* path );
void Pan_FreeSound( struct SYSTEM* sys, struct Sound* sound );
void Pan_PlaySound( struct SYSTEM* sys, struct Sound* sound );
void Pan_SoundVolume( struct Sound* sound, int volume );
void Pan_AllSoundVolume( struct SYSTEM* sys, int volume );

struct Music Pan_TryLoadMusic( struct SYSTEM* sys, const char* path, const char* ext );
void Pan_FreeMusic( struct SYSTEM* sys, struct Music* music );
void Pan_PlayMusic( struct SYSTEM* sys, struct Music* music );
void Pan_StopMusic( struct SYSTEM* sys );
void Pan_PauseMusic( struct SYSTEM* sys );
void Pan_ResumeMusic( struct SYSTEM* sys );
void Pan_ToggleMusic( struct SYSTEM* sys );
void Pan_MusicVolume( struct SYSTEM* sys, int volume );

struct MenuText Pan_SetupMenuText( struct SYSTEM* sys, char text[25], int x, int y, struct Rect basePosition, struct Font* font );
void Pan_DrawMenuText( struct SYSTEM* sys, struct MenuText* menuText );

struct MenuIcon Pan_SetupMenuIcon( struct SYSTEM* sys, struct TextureTable* textureTable, int index, int x, int y, int width, int height, struct Rect* basePosition );
void Pan_DrawMenuIcon( struct SYSTEM* sys, struct MenuIcon* menuIcon );

struct MenuButton Pan_SetupMenuButton( struct SYSTEM* sys, struct Texture* texture, int x, int y, int width, int height );
void Pan_DrawMenuButton( struct SYSTEM* sys, struct MenuButton* menuButton );

void Pan_DrawRectangle( struct SYSTEM* sys, struct Rect rectangle, int shade );
void Pan_FillRectangle( struct SYSTEM* sys, struct Rect rectangle, int shade );

#endif
