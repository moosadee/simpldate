#include "StoreState.h"
#include "../../omelet_engine/pan_system.h"
#include "../../omelet_engine/Structures.h"
#include "../../omelet_engine/Utilities.h"

// !! This isn't for a real money store, this is for character customation
//    and unlockable items using in-game points !!

struct {

    struct {
        struct Texture purchaseBackground;
        struct Texture tempStoney;
        struct TextureTable tabsTable;
    } GFX;

    struct MenuButton options[16];
    int totalOptions;
    int selectedOption;

} State_Store_Data;

void State_Store_Init( struct SYSTEM* sys, struct State* state )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

    state->Setup        = State_Store_Setup;
    state->Teardown     = State_Store_Teardown;
    state->HandleInput  = State_Store_HandleInput;
    state->Update       = State_Store_Update;
    state->Draw         = State_Store_Draw;
    state->CycleBegin   = State_Store_CycleBegin;
    state->CycleEnd     = State_Store_CycleEnd;
}

void State_Store_Setup( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

    State_Store_Data.GFX.purchaseBackground = Pan_TryLoadTexture( sys, "assets/graphics/purchase-icon.png" );
    State_Store_Data.GFX.tempStoney    = Pan_TryLoadTexture( sys, "assets/graphics/stoney-moose.png" );
    State_Store_Data.GFX.tabsTable     = Pan_TryLoadTextureTable( sys, "assets/graphics/tabs", 204, 19, "png" );

    State_Store_Data.totalOptions = 16;
    State_Store_Data.selectedOption = 0;

    State_Store_Data.options[0] = Pan_SetupMenuButton( sys, &sys->GLOBAL.buttonBackground, 25, 200, 160, 30 );
    State_Store_Data.options[0].menuText = Pan_SetupMenuText( sys, "Back",                 30, 7, State_Store_Data.options[0].position, &sys->GLOBAL.mediumFont );
    State_Store_Data.options[0].menuIcon = Pan_SetupMenuIcon( sys, &sys->GLOBAL.menuIcons, 5,  5, 7, 16, 16, &State_Store_Data.options[0].position );

    int x = 0, y = 0;
    int spaceW = 55, spaceH = 55;
    for ( int i = 1; i < State_Store_Data.totalOptions; i++ )
    {
        State_Store_Data.options[i] = Pan_SetupMenuButton( sys, &State_Store_Data.GFX.purchaseBackground, (x * spaceW) + 10, (y * spaceH) + 35, 37, 34 );
        State_Store_Data.options[i].menuText = Pan_SetupMenuText( sys, "100", 7, 23, State_Store_Data.options[i].position, &sys->GLOBAL.smallFont );

        x++;
        if ( x == 5 )
        {
            x = 0;
            y++;
        }
    }

//  State_Store_Data.options[2] = Pan_SetupMenuButton( sys, NULL, 50, 100 + ( 1 * ( 40 + 5 ) ), 160, 30 );
//  State_Store_Data.options[2].menuText = Pan_SetupMenuText( sys, "qwerqwer",    30, 7, &State_Store_Data.options[2].position, &sys->GLOBAL.mediumFont );
//  State_Store_Data.options[2].menuIcon = Pan_SetupMenuIcon( sys, &sys->GLOBAL.menuIcons, 16, 5, 7, 16, 16, &State_Store_Data.options[2].position );
}

void State_Store_Teardown( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
}

void State_Store_HandleInput( struct SYSTEM* sys )
{
    // Up/down to navigate
    if      ( sys->input.buttonUp      == BEGIN_RELEASE )
    {
        State_Store_Data.selectedOption--;
    }
    else if ( sys->input.buttonDown    == BEGIN_RELEASE )
    {
        State_Store_Data.selectedOption++;
    }

    // Keep cursor between the options
    if ( State_Store_Data.selectedOption < 0 )                              { State_Store_Data.selectedOption = State_Store_Data.totalOptions-1; }
    if ( State_Store_Data.selectedOption >= State_Store_Data.totalOptions ) { State_Store_Data.selectedOption = 0; }

    // Pressed a button
    if ( sys->input.buttonActionR == BEGIN_RELEASE )
    {
        if ( State_Store_Data.selectedOption == 0 )
        {
        }
        else if ( State_Store_Data.selectedOption == 1 )
        {
        }
        else if ( State_Store_Data.selectedOption == 2 )
        {
            strcpy( sys->nextState, "TitleState" );
        }
    }
}

void State_Store_Update( struct SYSTEM* sys )
{
}

void State_Store_Draw( struct SYSTEM* sys )
{
    struct Rect temp;// = { 0, 0, 400, 240 };
    temp.x = 0;
    temp.y = 0;
    temp.w = 400;
    temp.h = 240;
    Pan_DrawTexture( sys, &sys->GLOBAL.menuBackground, &temp, &temp );
//  temp.x = 10;
//  temp.y = 25;
//  temp.w = 350;
//  temp.h = 50;
//  Pan_DrawText( sys, &sys->GLOBAL.largeFont, "Store", &temp );

    temp.x = 3;
    temp.y = 3;
    temp.w = 204;
    temp.h = 19;
    Pan_DrawTextureTable( sys, &State_Store_Data.GFX.tabsTable, &temp, 0 );

    temp.x = 284;
    temp.y = 33;
    temp.w = 110;
    temp.h = 160;
    Pan_DrawTexture( sys, &State_Store_Data.GFX.tempStoney, NULL, &temp );


    for ( int i = 0; i < State_Store_Data.totalOptions; i++ )
    {
        Pan_DrawMenuButton( sys, &State_Store_Data.options[i] );
        if ( State_Store_Data.selectedOption == i )
        {
            struct Rect cursorPos;
            cursorPos.x = State_Store_Data.options[i].position.x - 25 - ( 2 - sys->globalInterval.current );
            cursorPos.y = State_Store_Data.options[i].position.y + 5;
            cursorPos.w = 24;
            cursorPos.h = 35;
            Pan_DrawTexture( sys, &sys->GLOBAL.cursor, NULL, &cursorPos );
        }
    }
}

void State_Store_CycleBegin( struct SYSTEM* sys )
{
}

void State_Store_CycleEnd( struct SYSTEM* sys )
{
}
