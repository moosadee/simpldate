#ifndef _MAPEDITOR_STATE
#define _MAPEDITOR_STATE

#include "../../omelet_engine/Structures.h"

void State_MapEditor_Init( struct SYSTEM* sys, struct State* state );

void State_MapEditor_Setup( struct SYSTEM* sys );
void State_MapEditor_Teardown( struct SYSTEM* sys );
void State_MapEditor_HandleInput( struct SYSTEM* sys );
void State_MapEditor_Update( struct SYSTEM* sys );
void State_MapEditor_Draw( struct SYSTEM* sys );
void State_MapEditor_CycleBegin( struct SYSTEM* sys );
void State_MapEditor_CycleEnd( struct SYSTEM* sys );

#endif
