#include "TemplateState.h"
#include "../../omelet_engine/pan_system.h"
#include "../../omelet_engine/Structures.h"
#include "../../omelet_engine/Utilities.h"

struct {
} State_Template_Data;

void State_Template_Init( struct SYSTEM* sys, struct State* state )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

    state->Setup        = State_Template_Setup;
    state->Teardown     = State_Template_Teardown;
    state->HandleInput  = State_Template_HandleInput;
    state->Update       = State_Template_Update;
    state->Draw         = State_Template_Draw;
    state->CycleBegin   = State_Template_CycleBegin;
    state->CycleEnd     = State_Template_CycleEnd;
}

void State_Template_Setup( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
}

void State_Template_Teardown( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
}

void State_Template_HandleInput( struct SYSTEM* sys )
{
}

void State_Template_Update( struct SYSTEM* sys )
{
}

void State_Template_Draw( struct SYSTEM* sys )
{
}

void State_Template_CycleBegin( struct SYSTEM* sys )
{
}

void State_Template_CycleEnd( struct SYSTEM* sys )
{
}
