#ifndef _TEMPLATE_STATE
#define _TEMPLATE_STATE

#include "../../omelet_engine/Structures.h"

void State_Options_Init( struct SYSTEM* sys, struct State* state );

void State_Options_Setup( struct SYSTEM* sys );
void State_Options_Teardown( struct SYSTEM* sys );
void State_Options_HandleInput( struct SYSTEM* sys );
void State_Options_Update( struct SYSTEM* sys );
void State_Options_Draw( struct SYSTEM* sys );
void State_Options_CycleBegin( struct SYSTEM* sys );
void State_Options_CycleEnd( struct SYSTEM* sys );

#endif
