#include "MapEditorState.h"
#include "../../omelet_engine/pan_system.h"
#include "../../omelet_engine/Structures.h"
#include "../../omelet_engine/Utilities.h"

struct {
    struct Map map;
    struct TextureTable playerTable;
    struct MenuText helpText[6];
    int helpTextCount;

    struct {
        struct Rect screenPosition;
        struct Rect tilePosition;
        float speed;
        struct Counter shade;
        struct Counter cooldown;
        struct MenuText text;
    } CURSOR;

    enum {
        MAP,
        TILES,
        OPTIONS
    } CURRENT_VIEW;
} State_MapEditor_Data;

void State_MapEditor_Init( struct SYSTEM* sys, struct State* state )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

    state->Setup        = State_MapEditor_Setup;
    state->Teardown     = State_MapEditor_Teardown;
    state->HandleInput  = State_MapEditor_HandleInput;
    state->Update       = State_MapEditor_Update;
    state->Draw         = State_MapEditor_Draw;
    state->CycleBegin   = State_MapEditor_CycleBegin;
    state->CycleEnd     = State_MapEditor_CycleEnd;
}

void State_MapEditor_Setup( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

    State_MapEditor_Data.playerTable = Pan_TryLoadTextureTable( sys, "assets/graphics/pickinsticks/rawr", 20, 20, "png" );

    // SETUP MAP EDIT SUBSTATE
    State_MapEditor_Data.map.textureTable = Pan_TryLoadTextureTable( sys, "assets/mapeditor/tileset", 20, 20, "png" );
    State_MapEditor_Data.map.tileDimensions.w = 20;
    State_MapEditor_Data.map.tileDimensions.h = 20;
    State_MapEditor_Data.map.dimensions.w = sys->renderBounds.w / State_MapEditor_Data.map.tileDimensions.w - 4; //80;
    State_MapEditor_Data.map.dimensions.h = sys->renderBounds.h / State_MapEditor_Data.map.tileDimensions.h; //60;
    State_MapEditor_Data.map.cameraOffset.x = 0;
    State_MapEditor_Data.map.cameraOffset.y = 0;

    State_MapEditor_Data.CURSOR.screenPosition.x = 0;
    State_MapEditor_Data.CURSOR.screenPosition.y = 0;
    State_MapEditor_Data.CURSOR.screenPosition.w = 20;
    State_MapEditor_Data.CURSOR.screenPosition.h = 20;
    State_MapEditor_Data.CURSOR.speed = 1.0;
    State_MapEditor_Data.CURSOR.shade.current   = 0;
    State_MapEditor_Data.CURSOR.shade.maximum   = 4;
    State_MapEditor_Data.CURSOR.shade.increment = 0.1;
    State_MapEditor_Data.CURSOR.cooldown.current   = 0;
    State_MapEditor_Data.CURSOR.cooldown.maximum   = 2;
    State_MapEditor_Data.CURSOR.cooldown.increment = 0.5;

    State_MapEditor_Data.CURSOR.text = Pan_SetupMenuText( sys, "", 0, 0, State_MapEditor_Data.CURSOR.screenPosition, &sys->GLOBAL.smallFont );
    sprintf( State_MapEditor_Data.CURSOR.text.text, "%d,%d", (int)(State_MapEditor_Data.CURSOR.tilePosition.x), (int)(State_MapEditor_Data.CURSOR.tilePosition.y) );

    State_MapEditor_Data.helpText[0] = Pan_SetupMenuText( sys, "1. Map",       320, 0, GetRect( 0, 0, 0, 0 ),  &sys->GLOBAL.smallFont );
    State_MapEditor_Data.helpText[1] = Pan_SetupMenuText( sys, "2. Tiles",     320, 20, GetRect( 0, 0, 0, 0 ), &sys->GLOBAL.smallFont );
    State_MapEditor_Data.helpText[2] = Pan_SetupMenuText( sys, "3. Options",   320, 40, GetRect( 0, 0, 0, 0 ), &sys->GLOBAL.smallFont );
    State_MapEditor_Data.helpText[3] = Pan_SetupMenuText( sys, "Current:",     320, 80, GetRect( 0, 0, 0, 0 ), &sys->GLOBAL.smallFont );
    State_MapEditor_Data.helpText[4] = Pan_SetupMenuText( sys, "A1: Place",    320, 120, GetRect( 0, 0, 0, 0 ), &sys->GLOBAL.smallFont );
    State_MapEditor_Data.helpText[5] = Pan_SetupMenuText( sys, "A2: Erase",    320, 140, GetRect( 0, 0, 0, 0 ), &sys->GLOBAL.smallFont );
    State_MapEditor_Data.helpTextCount = 6;

    State_MapEditor_Data.CURRENT_VIEW = MAP;


    // SETUP TILE SELECT SUBSTATE
//  State_MapEditor_Data.options[0] = Pan_SetupMenuButton( sys, NULL, 25, 100 + ( 0 * ( 40 + 5 ) ), 160, 30 );
//  State_MapEditor_Data.options[0].menuText = Pan_SetupMenuText( sys, "Music volume",    30, 7, State_Options_Data.options[0].position, &sys->GLOBAL.mediumFont );
//  State_MapEditor_Data.options[0].menuIcon = Pan_SetupMenuIcon( sys, &sys->GLOBAL.menuIcons, 15, 5, 7, 16, 16, &State_Options_Data.options[0].position );


    //State_MapEditor_Data.map.tiles = new struct MapTile*[ State_MapEditor_Data.map.dimensions.h ];
//  State_MapEditor_Data.map.tiles = (MapTile*) malloc( sizeof( MapTile* * State_MapEditor_Data.map.dimensions.h ) );
//  for ( int y = 0; y < State_MapEditor_Data.map.dimensions.h; y++ )
//  {
//    State_MapEditor_Data.map.tiles[y] = new struct MapTile[ State_MapEditor_Data.map.dimensions.w ];
//  }

    int tilewh = State_MapEditor_Data.map.tileDimensions.w;
    for ( int y = 0; y < State_MapEditor_Data.map.dimensions.h; y++ )
    {
        for ( int x = 0; x < State_MapEditor_Data.map.dimensions.w; x++ )
        {
            State_MapEditor_Data.map.tiles[x][y].textureTable = &State_MapEditor_Data.map.textureTable;
            State_MapEditor_Data.map.tiles[x][y].position.x = x * tilewh;
            State_MapEditor_Data.map.tiles[x][y].position.y = y * tilewh;
            State_MapEditor_Data.map.tiles[x][y].position.w = tilewh;
            State_MapEditor_Data.map.tiles[x][y].position.h = tilewh;
            State_MapEditor_Data.map.tiles[x][y].frameRect.x = x;
            State_MapEditor_Data.map.tiles[x][y].frameRect.y = 0;
            State_MapEditor_Data.map.tiles[x][y].frameRect.w = tilewh;
            State_MapEditor_Data.map.tiles[x][y].frameRect.h = tilewh;
        }
    }
}

void State_MapEditor_Teardown( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
//
//  for ( int y = 0; y < State_MapEditor_Data.map.dimensions.h; y++ )
//  {
//    delete [] State_MapEditor_Data.map.tiles[y];
//  }
}

void State_MapEditor_HandleInput( struct SYSTEM* sys )
{
    if ( State_MapEditor_Data.CURRENT_VIEW == MAP )
    {
        int altX = 0;
        int altY = 0;

        if      ( sys->input.buttonLeft    == BEGIN_PUSH || sys->input.buttonLeft    == HELD_DOWN )
        {
            altX = -1;
        }
        else if ( sys->input.buttonRight   == BEGIN_PUSH || sys->input.buttonRight   == HELD_DOWN )
        {
            altX = 1;
        }
        else if ( sys->input.buttonUp      == BEGIN_PUSH || sys->input.buttonUp      == HELD_DOWN )
        {
            altY = -1;
        }
        else if ( sys->input.buttonDown    == BEGIN_PUSH || sys->input.buttonDown    == HELD_DOWN )
        {
            altY = 1;
        }

        if ( State_MapEditor_Data.CURSOR.cooldown.current == 0 )
        {
            State_MapEditor_Data.CURSOR.tilePosition.x += altX;
            State_MapEditor_Data.CURSOR.tilePosition.y += altY;
        }

        if ( State_MapEditor_Data.CURSOR.tilePosition.x < 0 ) { State_MapEditor_Data.CURSOR.tilePosition.x = 0; }
        if ( State_MapEditor_Data.CURSOR.tilePosition.y < 0 ) { State_MapEditor_Data.CURSOR.tilePosition.y = 0; }
        if ( State_MapEditor_Data.CURSOR.tilePosition.x >= State_MapEditor_Data.map.dimensions.w ) { State_MapEditor_Data.CURSOR.tilePosition.x = State_MapEditor_Data.map.dimensions.w - 1; }
        if ( State_MapEditor_Data.CURSOR.tilePosition.y >= State_MapEditor_Data.map.dimensions.h ) { State_MapEditor_Data.CURSOR.tilePosition.y = State_MapEditor_Data.map.dimensions.h - 1; }

        State_MapEditor_Data.CURSOR.screenPosition.x = State_MapEditor_Data.CURSOR.tilePosition.x * State_MapEditor_Data.map.tileDimensions.w;
        State_MapEditor_Data.CURSOR.screenPosition.y = State_MapEditor_Data.CURSOR.tilePosition.y * State_MapEditor_Data.map.tileDimensions.h;

        //  State_MapEditor_Data.map.cameraOffset.x = ( State_MapEditor_Data.CURSOR.screenPosition.x + State_MapEditor_Data.CURSOR.screenPosition.w/2 ) - (sys->renderBounds.x + sys->renderBounds.w/2);
        //  State_MapEditor_Data.map.cameraOffset.y =  ( State_MapEditor_Data.CURSOR.screenPosition.y + State_MapEditor_Data.CURSOR.screenPosition.h/2 ) - (sys->renderBounds.y + sys->renderBounds.h/2);

        struct Rect offsetRect = GetOffsetRect( State_MapEditor_Data.CURSOR.screenPosition, State_MapEditor_Data.map.cameraOffset );
        State_MapEditor_Data.CURSOR.text = Pan_SetupMenuText( sys, "", 0, 0, offsetRect, &sys->GLOBAL.smallFont );
        sprintf( State_MapEditor_Data.CURSOR.text.text, "%d,%d", (int)(State_MapEditor_Data.CURSOR.tilePosition.x), (int)(State_MapEditor_Data.CURSOR.tilePosition.y) );
    }
}

void State_MapEditor_Update( struct SYSTEM* sys )
{
    if ( State_MapEditor_Data.CURRENT_VIEW == MAP )
    {
        Pan_IncrementCounter( &State_MapEditor_Data.CURSOR.shade );
        Pan_IncrementCounter( &State_MapEditor_Data.CURSOR.cooldown );
    }
}

void State_MapEditor_Draw( struct SYSTEM* sys )
{
    if ( State_MapEditor_Data.CURRENT_VIEW == MAP )
    {
        Pan_FillRectangle( sys, sys->renderBounds, 1 );
        struct Rect offsetRect;
        for ( int y = 0; y < State_MapEditor_Data.map.dimensions.h; y++ )
        {
            for ( int x = 0; x < State_MapEditor_Data.map.dimensions.w; x++ )
            {
                (*State_MapEditor_Data.map.tiles[x][y].textureTable).frameRect = State_MapEditor_Data.map.tiles[x][y].frameRect;
                offsetRect = GetOffsetRect( State_MapEditor_Data.map.tiles[x][y].position, State_MapEditor_Data.map.cameraOffset );
                Pan_DrawTextureTable( sys, State_MapEditor_Data.map.tiles[x][y].textureTable, &offsetRect, State_MapEditor_Data.map.tiles[x][y].frameRect.x );
            }
        }

        offsetRect = GetOffsetRect( State_MapEditor_Data.CURSOR.screenPosition, State_MapEditor_Data.map.cameraOffset );
        Pan_DrawRectangle( sys, offsetRect, State_MapEditor_Data.CURSOR.shade.current );

        Pan_DrawMenuText( sys, &State_MapEditor_Data.CURSOR.text );

        //  Pan_FillRectangle( sys, GetRect( 400-160, 0, 160, 240 ), 1 );
        for ( int i = 0; i < State_MapEditor_Data.helpTextCount; i++ )
        {
            Pan_DrawMenuText( sys, &State_MapEditor_Data.helpText[i] );
        }
    }
}

void State_MapEditor_CycleBegin( struct SYSTEM* sys )
{
}

void State_MapEditor_CycleEnd( struct SYSTEM* sys )
{
}
