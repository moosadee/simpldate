#include "OptionsState.h"
#include "../../omelet_engine/pan_system.h"
#include "../../omelet_engine/Structures.h"
#include "../../omelet_engine/Utilities.h"

#include <stdio.h>

struct {
    struct {
        struct Music music;
        struct Sound sound;
    } SFX;

    struct MenuButton options[7];
    int totalOptions;
    int selectedOption;

    struct MenuText text[2];
    int totalText;

} State_Options_Data;

void State_Options_Init( struct SYSTEM* sys, struct State* state )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

    state->Setup        = State_Options_Setup;
    state->Teardown     = State_Options_Teardown;
    state->HandleInput  = State_Options_HandleInput;
    state->Update       = State_Options_Update;
    state->Draw         = State_Options_Draw;
    state->CycleBegin   = State_Options_CycleBegin;
    state->CycleEnd     = State_Options_CycleEnd;
}

void State_Options_Setup( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

    State_Options_Data.SFX.sound       = Pan_TryLoadSound( sys, "assets/audio/sound1.wav" );
    State_Options_Data.SFX.music       = Pan_TryLoadMusic( sys, "assets/audio/song_dude", "wav" );

    State_Options_Data.totalOptions = 7;
    State_Options_Data.selectedOption = 0;
    State_Options_Data.totalText = 2;

    State_Options_Data.options[0] = Pan_SetupMenuButton( sys, NULL, 25, 100 + ( 0 * ( 40 + 5 ) ), 160, 30 );
    State_Options_Data.options[0].menuText = Pan_SetupMenuText( sys, "Music volume",    30, 7, State_Options_Data.options[0].position, &sys->GLOBAL.mediumFont );
    State_Options_Data.options[0].menuIcon = Pan_SetupMenuIcon( sys, &sys->GLOBAL.menuIcons, 15, 5, 7, 16, 16, &State_Options_Data.options[0].position );

    State_Options_Data.options[1] = Pan_SetupMenuButton( sys, NULL, 260, 100 + ( 0 * ( 40 + 5 ) ), 160, 30 );
    State_Options_Data.options[1].menuText = Pan_SetupMenuText( sys, "-",    5, 7, State_Options_Data.options[1].position, &sys->GLOBAL.mediumFont );
    State_Options_Data.options[1].menuIcon = Pan_SetupMenuIcon( sys, &sys->GLOBAL.menuIcons, 13, 5, 7, 16, 16, &State_Options_Data.options[1].position );

    State_Options_Data.options[2] = Pan_SetupMenuButton( sys, NULL, 350, 100 + ( 0 * ( 40 + 5 ) ), 160, 30 );
    State_Options_Data.options[2].menuText = Pan_SetupMenuText( sys, "+",    5, 7, State_Options_Data.options[2].position, &sys->GLOBAL.mediumFont );
    State_Options_Data.options[2].menuIcon = Pan_SetupMenuIcon( sys, &sys->GLOBAL.menuIcons, 13, 5, 7, 16, 16, &State_Options_Data.options[2].position );


    State_Options_Data.options[3] = Pan_SetupMenuButton( sys, NULL, 25, 100 + ( 1 * ( 40 + 5 ) ), 160, 30 );
    State_Options_Data.options[3].menuText = Pan_SetupMenuText( sys, "Sound volume",    30, 7, State_Options_Data.options[3].position, &sys->GLOBAL.mediumFont );
    State_Options_Data.options[3].menuIcon = Pan_SetupMenuIcon( sys, &sys->GLOBAL.menuIcons, 16, 5, 7, 16, 16, &State_Options_Data.options[3].position );

    State_Options_Data.options[4] = Pan_SetupMenuButton( sys, NULL, 260, 100 + ( 1 * ( 40 + 5 ) ), 160, 30 );
    State_Options_Data.options[4].menuText = Pan_SetupMenuText( sys, "-",    5, 7, State_Options_Data.options[4].position, &sys->GLOBAL.mediumFont );
    State_Options_Data.options[4].menuIcon = Pan_SetupMenuIcon( sys, &sys->GLOBAL.menuIcons, 13, 5, 7, 16, 16, &State_Options_Data.options[4].position );

    State_Options_Data.options[5] = Pan_SetupMenuButton( sys, NULL, 350, 100 + ( 1 * ( 40 + 5 ) ), 160, 30 );
    State_Options_Data.options[5].menuText = Pan_SetupMenuText( sys, "+",    5, 7, State_Options_Data.options[5].position, &sys->GLOBAL.mediumFont );
    State_Options_Data.options[5].menuIcon = Pan_SetupMenuIcon( sys, &sys->GLOBAL.menuIcons, 13, 5, 7, 16, 16, &State_Options_Data.options[5].position );

    State_Options_Data.options[6] = Pan_SetupMenuButton( sys, &sys->GLOBAL.buttonBackground, 50, 100 + ( 2 * ( 40 + 5 ) ), 160, 30 );
    State_Options_Data.options[6].menuText = Pan_SetupMenuText( sys, "Back",            30, 7, State_Options_Data.options[6].position, &sys->GLOBAL.mediumFont );
    State_Options_Data.options[6].menuIcon = Pan_SetupMenuIcon( sys, &sys->GLOBAL.menuIcons, 5,  5, 7, 16, 16, &State_Options_Data.options[6].position );

    State_Options_Data.text[0] = Pan_SetupMenuText( sys, "", 275, 7, State_Options_Data.options[0].position, &sys->GLOBAL.mediumFont );
    sprintf( State_Options_Data.text[0].text, "%d", (int)(sys->musicVolume.current/sys->musicVolume.maximum * 100) );

    State_Options_Data.text[1] = Pan_SetupMenuText( sys, "", 275, 7, State_Options_Data.options[3].position, &sys->GLOBAL.mediumFont );
    sprintf( State_Options_Data.text[1].text, "%d", (int)(sys->soundVolume.current/sys->soundVolume.maximum * 100) );
}

void State_Options_Teardown( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    Pan_StopMusic( sys );
    Pan_FreeSound( sys, &State_Options_Data.SFX.sound );
    Pan_FreeMusic( sys, &State_Options_Data.SFX.music );
}

void State_Options_HandleInput( struct SYSTEM* sys )
{
    // Up/down to navigate
    if      ( sys->input.buttonUp      == BEGIN_RELEASE )
    {
        State_Options_Data.selectedOption--;
    }
    else if ( sys->input.buttonDown    == BEGIN_RELEASE )
    {
        State_Options_Data.selectedOption++;
    }

    // Keep cursor between the options
    if ( State_Options_Data.selectedOption < 0 )                              { State_Options_Data.selectedOption = State_Options_Data.totalOptions-1; }
    if ( State_Options_Data.selectedOption >= State_Options_Data.totalOptions ) { State_Options_Data.selectedOption = 0; }

    bool altSelect = false;
#if defined(TARGET_SDL)
    // Check if mouse clicked a button (SDL)
    for ( int i = 0; i < State_Options_Data.totalOptions; i++ )
    {
        if ( PointInBox( sys->mouseX, sys->mouseY, State_Options_Data.options[i].position ) )
        {
            State_Options_Data.selectedOption = i;
        }
    }

    altSelect = sys->mouseDown;
#endif

    // Left/right to adjust
    char action[25] = "";
    if      ( sys->input.buttonLeft == BEGIN_RELEASE )
    {
        // Volume down
        if      ( State_Options_Data.selectedOption == 0 ) { strcpy( action, "mus-down" ); }
        else if ( State_Options_Data.selectedOption == 3 ) { strcpy( action, "snd-down" ); }
    }
    else if ( sys->input.buttonRight == BEGIN_RELEASE )
    {
        // Volume up
        if      ( State_Options_Data.selectedOption == 0 ) { strcpy( action, "mus-up" ); }
        else if ( State_Options_Data.selectedOption == 3 ) { strcpy( action, "snd-up" ); }
    }
    else if ( sys->input.buttonActionR == BEGIN_RELEASE || altSelect )
    {
        if      ( State_Options_Data.selectedOption == 1 ) { strcpy( action, "mus-down" ); }
        else if ( State_Options_Data.selectedOption == 2 ) { strcpy( action, "mus-up" ); }
        else if ( State_Options_Data.selectedOption == 4 ) { strcpy( action, "snd-down" ); }
        else if ( State_Options_Data.selectedOption == 5 ) { strcpy( action, "snd-up" ); }
        else if ( State_Options_Data.selectedOption == 6 ) { strcpy( sys->nextState, "TitleState" ); }
    }

    if ( strcmp( action, "mus-down" ) == 0)
    {
        Pan_MusicVolume( sys, sys->musicVolume.current - sys->musicVolume.increment );
        sprintf( State_Options_Data.text[0].text, "%d", (int)(sys->musicVolume.current/sys->musicVolume.maximum * 100) );
        Pan_PlayMusic( sys, &State_Options_Data.SFX.music );
    }
    else if ( strcmp( action, "mus-up" ) == 0 )
    {
        Pan_MusicVolume( sys, sys->musicVolume.current + sys->musicVolume.increment );
        sprintf( State_Options_Data.text[0].text, "%d", (int)(sys->musicVolume.current/sys->musicVolume.maximum * 100) );
        Pan_PlayMusic( sys, &State_Options_Data.SFX.music );
    }
    else if ( strcmp( action, "snd-down" ) == 0 )
    {
        Pan_AllSoundVolume( sys, sys->soundVolume.current - sys->soundVolume.increment );
        Pan_SoundVolume( &State_Options_Data.SFX.sound, sys->soundVolume.current );
        sprintf( State_Options_Data.text[1].text, "%d", (int)(sys->soundVolume.current/sys->soundVolume.maximum * 100) );
        Pan_PlaySound( sys, &State_Options_Data.SFX.sound );
    }
    else if ( strcmp( action, "snd-up" ) == 0 )
    {
        Pan_AllSoundVolume( sys, sys->soundVolume.current + sys->soundVolume.increment );
        Pan_SoundVolume( &State_Options_Data.SFX.sound, sys->soundVolume.current );
        sprintf( State_Options_Data.text[1].text, "%d", (int)(sys->soundVolume.current/sys->soundVolume.maximum * 100) );
        Pan_PlaySound( sys, &State_Options_Data.SFX.sound );
    }
}

void State_Options_Update( struct SYSTEM* sys )
{
}

void State_Options_Draw( struct SYSTEM* sys )
{
    struct Rect temp;// = { 0, 0, 400, 240 };
    temp.x = 0;
    temp.y = 0;
    temp.w = 400;
    temp.h = 240;
    Pan_DrawTexture( sys, &sys->GLOBAL.menuBackground, &temp, &temp );
    temp.x = 10;
    temp.y = 25;
    temp.w = 350;
    temp.h = 50;
    Pan_DrawText( sys, &sys->GLOBAL.largeFont, "Options", &temp );

    for ( int i = 0; i < State_Options_Data.totalOptions; i++ )
    {
        Pan_DrawMenuButton( sys, &State_Options_Data.options[i] );
        if ( State_Options_Data.selectedOption == i )
        {
            struct Rect cursorPos;
            cursorPos.x = State_Options_Data.options[i].position.x - 25 - ( 2 - sys->globalInterval.current );
            cursorPos.y = State_Options_Data.options[i].position.y + 5;
            cursorPos.w = 24;
            cursorPos.h = 35;
            Pan_DrawTexture( sys, &sys->GLOBAL.cursor, NULL, &cursorPos );
        }
    }

    for ( int i = 0; i < State_Options_Data.totalText; i++ )
    {
        Pan_DrawMenuText( sys, &State_Options_Data.text[i] );
    }
}

void State_Options_CycleBegin( struct SYSTEM* sys )
{
}

void State_Options_CycleEnd( struct SYSTEM* sys )
{
}
