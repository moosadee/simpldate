#ifndef _PICKIN_STICKS_TITLE_STATE
#define _PICKIN_STICKS_TITLE_STATE

#include "../../omelet_engine/Structures.h"

void State_PickinSticks_Title_Init( struct SYSTEM* sys, struct State* state );

void State_PickinSticks_Title_Setup( struct SYSTEM* sys );
void State_PickinSticks_Title_Teardown( struct SYSTEM* sys );
void State_PickinSticks_Title_HandleInput( struct SYSTEM* sys );
void State_PickinSticks_Title_Update( struct SYSTEM* sys );
void State_PickinSticks_Title_Draw( struct SYSTEM* sys );
void State_PickinSticks_Title_CycleBegin( struct SYSTEM* sys );
void State_PickinSticks_Title_CycleEnd( struct SYSTEM* sys );

#endif
