#ifndef _STORE_STATE
#define _STORE_STATE

#include "../../omelet_engine/Structures.h"

void State_Store_Init( struct SYSTEM* sys, struct State* state );

void State_Store_Setup( struct SYSTEM* sys );
void State_Store_Teardown( struct SYSTEM* sys );
void State_Store_HandleInput( struct SYSTEM* sys );
void State_Store_Update( struct SYSTEM* sys );
void State_Store_Draw( struct SYSTEM* sys );
void State_Store_CycleBegin( struct SYSTEM* sys );
void State_Store_CycleEnd( struct SYSTEM* sys );

#endif
