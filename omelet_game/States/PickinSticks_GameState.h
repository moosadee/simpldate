#ifndef _PICKIN_STICKS_GAME_STATE
#define _PICKIN_STICKS_GAME_STATE

#include "../../omelet_engine/Structures.h"

void State_PickinSticks_Game_Init( struct SYSTEM* sys, struct State* state );

void State_PickinSticks_Game_Setup( struct SYSTEM* sys );
void State_PickinSticks_Game_Teardown( struct SYSTEM* sys );
void State_PickinSticks_Game_HandleInput( struct SYSTEM* sys );
void State_PickinSticks_Game_Update( struct SYSTEM* sys );
void State_PickinSticks_Game_Draw( struct SYSTEM* sys );
void State_PickinSticks_Game_CycleBegin( struct SYSTEM* sys );
void State_PickinSticks_Game_CycleEnd( struct SYSTEM* sys );

#endif
