#include "TitleState.h"
#include "../../omelet_engine/pan_system.h"
#include "../../omelet_engine/Structures.h"
#include "../../omelet_engine/Utilities.h"

#include <stdio.h>

struct {

    struct MenuButton options[10];
    int totalOptions;
    int selectedOption;
    struct MenuText text[1];
    int totalText;

} State_Title_Data;

void State_Title_Init( struct SYSTEM* sys, struct State* state )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

    state->Setup        = State_Title_Setup;
    state->Teardown     = State_Title_Teardown;
    state->HandleInput  = State_Title_HandleInput;
    state->Update       = State_Title_Update;
    state->Draw         = State_Title_Draw;
    state->CycleBegin   = State_Title_CycleBegin;
    state->CycleEnd     = State_Title_CycleEnd;

    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}

void State_Title_Setup( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

    State_Title_Data.totalOptions = 10;
    State_Title_Data.selectedOption = 0;

    State_Title_Data.options[0] = Pan_SetupMenuButton( sys, &sys->GLOBAL.buttonBackground, 15, 75 + ( 0 * ( 40 + 5 ) ), 160, 30 );
    State_Title_Data.options[0].menuText = Pan_SetupMenuText( sys, "PS Demo",    30, 7, State_Title_Data.options[0].position, &sys->GLOBAL.mediumFont );
    State_Title_Data.options[0].menuIcon = Pan_SetupMenuIcon( sys, &sys->GLOBAL.menuIcons, 0, 5, 7, 16, 16, &State_Title_Data.options[0].position );

    State_Title_Data.options[1] = Pan_SetupMenuButton( sys, &sys->GLOBAL.buttonBackground, 15, 75 + ( 1 * ( 40 + 5 ) ), 160, 30 );
    State_Title_Data.options[1].menuText = Pan_SetupMenuText( sys, "Mapedit", 30, 7, State_Title_Data.options[1].position, &sys->GLOBAL.mediumFont );
    State_Title_Data.options[1].menuIcon = Pan_SetupMenuIcon( sys, &sys->GLOBAL.menuIcons, 1, 5, 7, 16, 16, &State_Title_Data.options[1].position );

    State_Title_Data.options[2] = Pan_SetupMenuButton( sys, &sys->GLOBAL.buttonBackground, 15, 75 + ( 2 * ( 40 + 5 ) ), 160, 30 );
    State_Title_Data.options[2].menuText = Pan_SetupMenuText( sys, "OPT3",    30, 7, State_Title_Data.options[2].position, &sys->GLOBAL.mediumFont );
    State_Title_Data.options[2].menuIcon = Pan_SetupMenuIcon( sys, &sys->GLOBAL.menuIcons, 4, 5, 7, 16, 16, &State_Title_Data.options[2].position );

    State_Title_Data.options[3] = Pan_SetupMenuButton( sys, &sys->GLOBAL.buttonBackground, 200, 75 + ( 0 * ( 40 + 5 ) ), 160, 30 );
    State_Title_Data.options[3].menuText = Pan_SetupMenuText( sys, "OPT4",    30, 7, State_Title_Data.options[3].position, &sys->GLOBAL.mediumFont );
    State_Title_Data.options[3].menuIcon = Pan_SetupMenuIcon( sys, &sys->GLOBAL.menuIcons, 0, 5, 7, 16, 16, &State_Title_Data.options[3].position );

    State_Title_Data.options[4] = Pan_SetupMenuButton( sys, &sys->GLOBAL.buttonBackground, 200, 75 + ( 1 * ( 40 + 5 ) ), 160, 30 );
    State_Title_Data.options[4].menuText = Pan_SetupMenuText( sys, "OPT5", 30, 7, State_Title_Data.options[4].position, &sys->GLOBAL.mediumFont );
    State_Title_Data.options[4].menuIcon = Pan_SetupMenuIcon( sys, &sys->GLOBAL.menuIcons, 1, 5, 7, 16, 16, &State_Title_Data.options[4].position );

    State_Title_Data.options[5] = Pan_SetupMenuButton( sys, &sys->GLOBAL.buttonBackground, 200, 75 + ( 2 * ( 40 + 5 ) ), 160, 30 );
    State_Title_Data.options[5].menuText = Pan_SetupMenuText( sys, "OPT6",    30, 7, State_Title_Data.options[5].position, &sys->GLOBAL.mediumFont );
    State_Title_Data.options[5].menuIcon = Pan_SetupMenuIcon( sys, &sys->GLOBAL.menuIcons, 4, 5, 7, 16, 16, &State_Title_Data.options[5].position );

    State_Title_Data.options[6] = Pan_SetupMenuButton( sys, NULL, 20, 210, 160, 30 );
    State_Title_Data.options[6].menuText = Pan_SetupMenuText( sys, "Shoppe", 30, 5, State_Title_Data.options[6].position, &sys->GLOBAL.smallFont );
    State_Title_Data.options[6].menuIcon = Pan_SetupMenuIcon( sys, &sys->GLOBAL.menuIcons, 17, 5, 0, 16, 16, &State_Title_Data.options[6].position );

    State_Title_Data.options[7] = Pan_SetupMenuButton( sys, NULL, 120, 210, 160, 30 );
    State_Title_Data.options[7].menuText = Pan_SetupMenuText( sys, "Options", 30, 5, State_Title_Data.options[7].position, &sys->GLOBAL.smallFont );
    State_Title_Data.options[7].menuIcon = Pan_SetupMenuIcon( sys, &sys->GLOBAL.menuIcons, 1, 5, 0, 16, 16, &State_Title_Data.options[7].position );

    State_Title_Data.options[8] = Pan_SetupMenuButton( sys, NULL, 220, 210, 160, 30 );
    State_Title_Data.options[8].menuText = Pan_SetupMenuText( sys, "Help", 30, 5, State_Title_Data.options[8].position, &sys->GLOBAL.smallFont );
    State_Title_Data.options[8].menuIcon = Pan_SetupMenuIcon( sys, &sys->GLOBAL.menuIcons, 2, 5, 0, 16, 16, &State_Title_Data.options[8].position );

    State_Title_Data.options[9] = Pan_SetupMenuButton( sys, NULL, 320, 210, 160, 30 );
    State_Title_Data.options[9].menuText = Pan_SetupMenuText( sys, "Quit",    30, 5, State_Title_Data.options[9].position, &sys->GLOBAL.smallFont );
    State_Title_Data.options[9].menuIcon = Pan_SetupMenuIcon( sys, &sys->GLOBAL.menuIcons, 4, 5, 0, 16, 16, &State_Title_Data.options[9].position );

    struct Rect tempRect;
    tempRect.x = 25;
    tempRect.y = 50;
    tempRect.w = tempRect.h = -1;
    State_Title_Data.totalText = 1;
    State_Title_Data.text[0] = Pan_SetupMenuText( sys, "WASD to navigate, J for cancel, K to confirm", 0, 0, tempRect, &sys->GLOBAL.smallFont );

    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}

void State_Title_Teardown( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}

void State_Title_HandleInput( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );


    if      ( sys->input.buttonUp      == BEGIN_RELEASE )
    {
        State_Title_Data.selectedOption--;
    }
    else if ( sys->input.buttonDown    == BEGIN_RELEASE )
    {
        State_Title_Data.selectedOption++;
    }

    // Keep cursor between the options
    if ( State_Title_Data.selectedOption < 0 )                              { State_Title_Data.selectedOption = State_Title_Data.totalOptions-1; }
    if ( State_Title_Data.selectedOption >= State_Title_Data.totalOptions ) { State_Title_Data.selectedOption = 0; }

    bool altSelect = false;
#if defined(TARGET_SDL)
    // Check if mouse clicked a button (SDL)
    // TODO: I'm going to need a menu manager eventually...
    for ( int i = 0; i < State_Title_Data.totalOptions; i++ )
    {
        if ( PointInBox( sys->mouseX, sys->mouseY, State_Title_Data.options[i].position ) )
        {
            State_Title_Data.selectedOption = i;
        }
    }
    altSelect = sys->mouseDown;
#endif

    // Pressed a button
    if ( sys->input.buttonActionR == BEGIN_RELEASE || altSelect )
    {
        if      ( State_Title_Data.selectedOption == 0 ) { strcpy( sys->nextState, "PickinSticks_TitleState" ); }
        else if ( State_Title_Data.selectedOption == 1 ) { strcpy( sys->nextState, "MapEditor_State" ); }
        else if ( State_Title_Data.selectedOption == 2 ) { strcpy( sys->nextState, "" ); }
        else if ( State_Title_Data.selectedOption == 3 ) { strcpy( sys->nextState, "" ); }
        else if ( State_Title_Data.selectedOption == 4 ) { strcpy( sys->nextState, "" ); }
        else if ( State_Title_Data.selectedOption == 5 ) { strcpy( sys->nextState, "" ); }
        else if ( State_Title_Data.selectedOption == 6 ) { strcpy( sys->nextState, "StoreState" ); }
        else if ( State_Title_Data.selectedOption == 7 ) { strcpy( sys->nextState, "OptionsState" ); }
        else if ( State_Title_Data.selectedOption == 8 ) { strcpy( sys->nextState, "" ); }
        else if ( State_Title_Data.selectedOption == 9 ) { sys->wantToQuit = true; }
    }

    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}

void State_Title_Update( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}

void State_Title_Draw( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

    struct Rect temp;//= { 0, 0, 400, 240 };
    temp.x = 0;
    temp.y = 0;
    temp.w = 400;
    temp.h = 240;
    Pan_DrawTexture( sys, &sys->GLOBAL.menuBackground, &temp, &temp );
    temp.x = 50;
    temp.y = 25;
    Pan_DrawText( sys, &sys->GLOBAL.mediumFont, "Omelet Engine Demo", &temp );

    // Option 0
    for ( int i = 0; i < State_Title_Data.totalOptions; i++ )
    {
//    Pan_LogValueInt( sys, __FILE__, __func__, __LINE__, VERBOSE, "i", i );
        Pan_DrawMenuButton( sys, &State_Title_Data.options[i] );
        if ( State_Title_Data.selectedOption == i )
        {
            struct Rect cursorPos;
            cursorPos.x = State_Title_Data.options[i].position.x - 20 + ( 4 - sys->globalInterval.current );
            cursorPos.y = State_Title_Data.options[i].position.y;
            cursorPos.w = 24;
            cursorPos.h = 35;
            Pan_DrawTexture( sys, &sys->GLOBAL.cursor, NULL, &cursorPos );
        }
    }

    for ( int i = 0; i < State_Title_Data.totalText; i++ )
    {
        Pan_DrawMenuText( sys, &State_Title_Data.text[i] );
    }

    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}

void State_Title_CycleBegin( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}

void State_Title_CycleEnd( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}
