#include "PickinSticks_TitleState.h"
#include "../../omelet_engine/pan_system.h"
#include "../../omelet_engine/Structures.h"
#include "../../omelet_engine/Utilities.h"

#include <stdio.h>

struct {

    struct {
        struct Texture background;
        struct Texture buttonBackground;
        struct Texture cursor;
        struct TextureTable menuIcons;
    } GFX;

    struct MenuButton options[3];
    int totalOptions;
    int selectedOption;

} State_PickinSticks_Title_Data;

void State_PickinSticks_Title_Init( struct SYSTEM* sys, struct State* state )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

    state->Setup        = State_PickinSticks_Title_Setup;
    state->Teardown     = State_PickinSticks_Title_Teardown;
    state->HandleInput  = State_PickinSticks_Title_HandleInput;
    state->Update       = State_PickinSticks_Title_Update;
    state->Draw         = State_PickinSticks_Title_Draw;
    state->CycleBegin   = State_PickinSticks_Title_CycleBegin;
    state->CycleEnd     = State_PickinSticks_Title_CycleEnd;

    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}

void State_PickinSticks_Title_Setup( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

    State_PickinSticks_Title_Data.GFX.background        = Pan_TryLoadTexture( sys, "assets/graphics/pickinsticks/titlebg.png" );
    State_PickinSticks_Title_Data.GFX.buttonBackground  = Pan_TryLoadTexture( sys, "assets/graphics/pickinsticks/buttonbg.png" );
    State_PickinSticks_Title_Data.GFX.cursor            = Pan_TryLoadTexture( sys, "assets/graphics/pickinsticks/cursor.png" );
    State_PickinSticks_Title_Data.GFX.menuIcons         = Pan_TryLoadTextureTable( sys, "assets/graphics/icons", 16, 16, "png" );

    State_PickinSticks_Title_Data.totalOptions = 3;
    State_PickinSticks_Title_Data.selectedOption = 0;

    State_PickinSticks_Title_Data.options[0] = Pan_SetupMenuButton( sys, &State_PickinSticks_Title_Data.GFX.buttonBackground, 15, 100 + ( 0 * ( 40 + 5 ) ), 160, 30 );
    State_PickinSticks_Title_Data.options[0].menuText = Pan_SetupMenuText( sys, "Play",    30, 7, State_PickinSticks_Title_Data.options[0].position, &sys->GLOBAL.mediumFont );
    State_PickinSticks_Title_Data.options[0].menuIcon = Pan_SetupMenuIcon( sys, &State_PickinSticks_Title_Data.GFX.menuIcons, 0, 5, 7, 16, 16, &State_PickinSticks_Title_Data.options[0].position );

    State_PickinSticks_Title_Data.options[1] = Pan_SetupMenuButton( sys, &State_PickinSticks_Title_Data.GFX.buttonBackground, 15, 100 + ( 1 * ( 40 + 5 ) ), 160, 30 );
    State_PickinSticks_Title_Data.options[1].menuText = Pan_SetupMenuText( sys, "Options", 30, 7, State_PickinSticks_Title_Data.options[1].position, &sys->GLOBAL.mediumFont );
    State_PickinSticks_Title_Data.options[1].menuIcon = Pan_SetupMenuIcon( sys, &State_PickinSticks_Title_Data.GFX.menuIcons, 1, 5, 7, 16, 16, &State_PickinSticks_Title_Data.options[1].position );

    State_PickinSticks_Title_Data.options[2] = Pan_SetupMenuButton( sys, &State_PickinSticks_Title_Data.GFX.buttonBackground, 15, 100 + ( 2 * ( 40 + 5 ) ), 160, 30 );
    State_PickinSticks_Title_Data.options[2].menuText = Pan_SetupMenuText( sys, "Exit",    30, 7, State_PickinSticks_Title_Data.options[2].position, &sys->GLOBAL.mediumFont );
    State_PickinSticks_Title_Data.options[2].menuIcon = Pan_SetupMenuIcon( sys, &State_PickinSticks_Title_Data.GFX.menuIcons, 4, 5, 7, 16, 16, &State_PickinSticks_Title_Data.options[2].position );

    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}

void State_PickinSticks_Title_Teardown( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

    Pan_FreeTexture( sys, &State_PickinSticks_Title_Data.GFX.background );
    Pan_FreeTexture( sys, &State_PickinSticks_Title_Data.GFX.cursor );
    Pan_FreeTextureTable( sys, &State_PickinSticks_Title_Data.GFX.menuIcons );

    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}

void State_PickinSticks_Title_HandleInput( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

    if      ( sys->input.buttonUp      == BEGIN_RELEASE )
    {
        State_PickinSticks_Title_Data.selectedOption--;
    }
    else if ( sys->input.buttonDown    == BEGIN_RELEASE )
    {
        State_PickinSticks_Title_Data.selectedOption++;
    }

    // Keep cursor between the options
    if ( State_PickinSticks_Title_Data.selectedOption < 0 )                              { State_PickinSticks_Title_Data.selectedOption = State_PickinSticks_Title_Data.totalOptions-1; }
    if ( State_PickinSticks_Title_Data.selectedOption >= State_PickinSticks_Title_Data.totalOptions ) { State_PickinSticks_Title_Data.selectedOption = 0; }

    // Pressed a button
    if ( sys->input.buttonActionR == BEGIN_RELEASE )
    {
        if ( State_PickinSticks_Title_Data.selectedOption == 0 )
        {
            strcpy( sys->nextState, "PickinSticks_GameState" );
        }
        else if ( State_PickinSticks_Title_Data.selectedOption == 1 )
        {
            strcpy( sys->nextState, "OptionsState" );
        }
        else if ( State_PickinSticks_Title_Data.selectedOption == 2 )
        {
            strcpy( sys->nextState, "TitleState" );
        }
    }
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}

void State_PickinSticks_Title_Update( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}

void State_PickinSticks_Title_Draw( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

    struct Rect temp;//= { 0, 0, 400, 240 };
    temp.x = 0;
    temp.y = 0;
    temp.w = 400;
    temp.h = 240;
    Pan_DrawTexture( sys, &State_PickinSticks_Title_Data.GFX.background, &temp, &temp );
    temp.x = 30;
    temp.y = 25;
//  Pan_DrawText( sys, &State_PickinSticks_Title_Data.FONTS.largeFont, "Pickin' Sticks", &temp );

    // Option 0
    for ( int i = 0; i < State_PickinSticks_Title_Data.totalOptions; i++ )
    {
//    Pan_LogValueInt( sys, __FILE__, __func__, __LINE__, VERBOSE, "i", i );
        Pan_DrawMenuButton( sys, &State_PickinSticks_Title_Data.options[i] );
        if ( State_PickinSticks_Title_Data.selectedOption == i )
        {
            struct Rect cursorPos;
            cursorPos.x = State_PickinSticks_Title_Data.options[i].position.x - 20 + ( 4 - sys->globalInterval.current );
            cursorPos.y = State_PickinSticks_Title_Data.options[i].position.y;
            cursorPos.w = 24;
            cursorPos.h = 35;
            Pan_DrawTexture( sys, &State_PickinSticks_Title_Data.GFX.cursor, NULL, &cursorPos );
        }
    }
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}

void State_PickinSticks_Title_CycleBegin( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}

void State_PickinSticks_Title_CycleEnd( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION END" );
}
