#ifndef _TITLE_STATE
#define _TITLE_STATE

#include "../../omelet_engine/Structures.h"

void State_Title_Init( struct SYSTEM* sys, struct State* state );

void State_Title_Setup( struct SYSTEM* sys );
void State_Title_Teardown( struct SYSTEM* sys );
void State_Title_HandleInput( struct SYSTEM* sys );
void State_Title_Update( struct SYSTEM* sys );
void State_Title_Draw( struct SYSTEM* sys );
void State_Title_CycleBegin( struct SYSTEM* sys );
void State_Title_CycleEnd( struct SYSTEM* sys );

#endif
