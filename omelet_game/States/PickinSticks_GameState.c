#include "PickinSticks_GameState.h"
#include "../../omelet_engine/pan_system.h"
#include "../../omelet_engine/Structures.h"
#include "../../omelet_engine/Utilities.h"

#include <stdio.h>

struct {

    struct {
        struct Texture background;
        struct Texture stick;
        struct TextureTable playerTable;
    } GFX;

    struct {
        struct Music music;
        struct Sound collect;
    } SFX;

    struct {
        int score;
        enum Direction direction;
        struct Rect position;
        float speed;
        struct Counter frame;
    } PLAYER;

    struct {
        struct Rect position;
    } STICK;

    struct {
        struct Rect position;
    };

    struct MenuText scoreText;

} State_PickinSticks_Game_Data;

void State_PickinSticks_Game_Init( struct SYSTEM* sys, struct State* state )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    state->Setup        = State_PickinSticks_Game_Setup;
    state->Teardown     = State_PickinSticks_Game_Teardown;
    state->HandleInput  = State_PickinSticks_Game_HandleInput;
    state->Update       = State_PickinSticks_Game_Update;
    state->Draw         = State_PickinSticks_Game_Draw;
    state->CycleBegin   = State_PickinSticks_Game_CycleBegin;
    state->CycleEnd     = State_PickinSticks_Game_CycleEnd;
}

void State_PickinSticks_Game_Setup( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );

    State_PickinSticks_Game_Data.GFX.playerTable = Pan_TryLoadTextureTable( sys, "assets/graphics/pickinsticks/rawr", 20, 20, "png" );
    State_PickinSticks_Game_Data.GFX.stick       = Pan_TryLoadTexture( sys, "assets/graphics/pickinsticks/stick.png" );
    State_PickinSticks_Game_Data.GFX.background  = Pan_TryLoadTexture( sys, "assets/graphics/pickinsticks/ground.png" );
    State_PickinSticks_Game_Data.SFX.collect     = Pan_TryLoadSound( sys, "assets/audio/sound1.wav" );
    State_PickinSticks_Game_Data.SFX.music       = Pan_TryLoadMusic( sys, "assets/audio/song_dude", "wav" );

    Pan_PlayMusic( sys, &State_PickinSticks_Game_Data.SFX.music );

    State_PickinSticks_Game_Data.PLAYER.frame.current = 0;
    State_PickinSticks_Game_Data.PLAYER.frame.maximum = 2;
    State_PickinSticks_Game_Data.PLAYER.frame.increment = 0.1;
    State_PickinSticks_Game_Data.PLAYER.position.w = 20;
    State_PickinSticks_Game_Data.PLAYER.position.h = 20;
    State_PickinSticks_Game_Data.PLAYER.position.x = sys->renderBounds.w/2 - State_PickinSticks_Game_Data.PLAYER.position.w/2;
    State_PickinSticks_Game_Data.PLAYER.position.y = sys->renderBounds.h/2 - State_PickinSticks_Game_Data.PLAYER.position.h/2;
    State_PickinSticks_Game_Data.PLAYER.score = 0;
    State_PickinSticks_Game_Data.PLAYER.speed = 2;
    State_PickinSticks_Game_Data.PLAYER.direction = SOUTH;

    State_PickinSticks_Game_Data.STICK.position.w = 20;
    State_PickinSticks_Game_Data.STICK.position.h = 20;
    SetRandomScreenPosition( sys, &State_PickinSticks_Game_Data.STICK.position );

    State_PickinSticks_Game_Data.scoreText = Pan_SetupMenuText( sys, "Score: 0", 10, 10, GetRect( 0, 0, 0, 0 ), &sys->GLOBAL.mediumFont );
    sprintf( State_PickinSticks_Game_Data.scoreText.text, "Score: %d", State_PickinSticks_Game_Data.PLAYER.score );
}

void State_PickinSticks_Game_Teardown( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    Pan_StopMusic( sys );
    Pan_FreeTexture( sys,  &State_PickinSticks_Game_Data.GFX.stick );
    Pan_FreeTexture( sys, &State_PickinSticks_Game_Data.GFX.background );
    Pan_FreeTextureTable( sys, &State_PickinSticks_Game_Data.GFX.playerTable );
    Pan_FreeSound( sys, &State_PickinSticks_Game_Data.SFX.collect );
    Pan_FreeMusic( sys, &State_PickinSticks_Game_Data.SFX.music );
}

void State_PickinSticks_Game_HandleInput( struct SYSTEM* sys )
{
}

void State_PickinSticks_Game_Update( struct SYSTEM* sys )
{
    Pan_IncrementCounter( &State_PickinSticks_Game_Data.PLAYER.frame );

    // Player movement
    if      ( sys->input.buttonLeft    == BEGIN_PUSH || sys->input.buttonLeft    == HELD_DOWN )
    {
        State_PickinSticks_Game_Data.PLAYER.position.x -= State_PickinSticks_Game_Data.PLAYER.speed;
        State_PickinSticks_Game_Data.PLAYER.direction = WEST;
    }
    else if ( sys->input.buttonRight   == BEGIN_PUSH || sys->input.buttonRight   == HELD_DOWN )
    {
        State_PickinSticks_Game_Data.PLAYER.position.x += State_PickinSticks_Game_Data.PLAYER.speed;
        State_PickinSticks_Game_Data.PLAYER.direction = EAST;
    }
    else if ( sys->input.buttonUp      == BEGIN_PUSH || sys->input.buttonUp      == HELD_DOWN )
    {
        State_PickinSticks_Game_Data.PLAYER.position.y -= State_PickinSticks_Game_Data.PLAYER.speed;
        State_PickinSticks_Game_Data.PLAYER.direction = NORTH;
    }
    else if ( sys->input.buttonDown    == BEGIN_PUSH || sys->input.buttonDown    == HELD_DOWN )
    {
        State_PickinSticks_Game_Data.PLAYER.position.y += State_PickinSticks_Game_Data.PLAYER.speed;
        State_PickinSticks_Game_Data.PLAYER.direction = SOUTH;
    }

    // Keep player on the screen
    if      ( State_PickinSticks_Game_Data.PLAYER.position.x < 0 )                                            { State_PickinSticks_Game_Data.PLAYER.position.x = 0; }
    else if ( State_PickinSticks_Game_Data.PLAYER.position.x > sys->renderBounds.w - State_PickinSticks_Game_Data.PLAYER.position.w ) { State_PickinSticks_Game_Data.PLAYER.position.x = sys->renderBounds.w - State_PickinSticks_Game_Data.PLAYER.position.w; }
    if      ( State_PickinSticks_Game_Data.PLAYER.position.y < 0 )                                            { State_PickinSticks_Game_Data.PLAYER.position.y = 0; }
    else if ( State_PickinSticks_Game_Data.PLAYER.position.y > sys->renderBounds.h - State_PickinSticks_Game_Data.PLAYER.position.h ) { State_PickinSticks_Game_Data.PLAYER.position.y = sys->renderBounds.h - State_PickinSticks_Game_Data.PLAYER.position.h; }

    // Collision with stick?
    if ( BoundingBoxCollision( State_PickinSticks_Game_Data.PLAYER.position, State_PickinSticks_Game_Data.STICK.position ) == true )
    {
        State_PickinSticks_Game_Data.PLAYER.score += 1;
        SetRandomScreenPosition( sys, &State_PickinSticks_Game_Data.STICK.position );
        Pan_PlaySound( sys, &State_PickinSticks_Game_Data.SFX.collect );
        sprintf( State_PickinSticks_Game_Data.scoreText.text, "Score: %d", State_PickinSticks_Game_Data.PLAYER.score );
    }
}

void State_PickinSticks_Game_Draw( struct SYSTEM* sys )
{
    struct Rect temp;//= { 0, 0, 400, 240 };
    temp.x = 0;
    temp.y = 0;
    temp.w = 400;
    temp.h = 240;

    Pan_DrawTexture( sys, &State_PickinSticks_Game_Data.GFX.background, &temp, &temp );

    temp.w = State_PickinSticks_Game_Data.STICK.position.w;
    temp.h = State_PickinSticks_Game_Data.STICK.position.h;
    Pan_DrawTexture( sys, &State_PickinSticks_Game_Data.GFX.stick, &temp, &State_PickinSticks_Game_Data.STICK.position );

    Pan_DrawTextureTable( sys, &State_PickinSticks_Game_Data.GFX.playerTable, &State_PickinSticks_Game_Data.PLAYER.position, (int)(State_PickinSticks_Game_Data.PLAYER.direction*2 + (int)State_PickinSticks_Game_Data.PLAYER.frame.current ) );

    Pan_DrawMenuText( sys, &State_PickinSticks_Game_Data.scoreText );
}

void State_PickinSticks_Game_CycleBegin( struct SYSTEM* sys )
{
}

void State_PickinSticks_Game_CycleEnd( struct SYSTEM* sys )
{
}
