#ifndef _TEMPLATE_STATE
#define _TEMPLATE_STATE

#include "../../omelet_engine/Structures.h"

void State_Template_Init( struct SYSTEM* sys, struct State* state );

void State_Template_Setup( struct SYSTEM* sys );
void State_Template_Teardown( struct SYSTEM* sys );
void State_Template_HandleInput( struct SYSTEM* sys );
void State_Template_Update( struct SYSTEM* sys );
void State_Template_Draw( struct SYSTEM* sys );
void State_Template_CycleBegin( struct SYSTEM* sys );
void State_Template_CycleEnd( struct SYSTEM* sys );

#endif
