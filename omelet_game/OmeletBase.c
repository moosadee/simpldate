//#include <stdio.h>
//#include <stdlib.h>

#include "../omelet_engine/pan_system.h"
#include "../omelet_engine/Structures.h"
#include "../omelet_engine/Utilities.h"
#include "OmeletBase.h"
#include "States/TitleState.h"
#include "States/OptionsState.h"
#include "States/StoreState.h"
#include "States/PickinSticks_GameState.h"
#include "States/PickinSticks_TitleState.h"
#include "States/MapEditorState.h"

void State_Title_Init( struct SYSTEM* sys, struct State* state );
void State_Options_Init( struct SYSTEM* sys, struct State* state );
void State_Game_Init( struct SYSTEM* sys, struct State* state );

void Setup( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    Pan_Setup( sys, 400, 240, 400*2, 240*2, "Omelet Engine", true );
//  Pan_Setup( sys, 400, 240, 400, 240, "Pickin' Sticks", true );

    sys->GLOBAL.largeFont   = Pan_TryLoadFont( sys, "assets/fonts/BitmooseMedium", 24, "png" );
    sys->GLOBAL.mediumFont  = Pan_TryLoadFont( sys, "assets/fonts/BitmooseSmall", 16, "png" );
    sys->GLOBAL.smallFont   = Pan_TryLoadFont( sys, "assets/fonts/BitmooseTiny", 8, "png" );

    sys->GLOBAL.menuBackground       = Pan_TryLoadTexture( sys, "assets/graphics/menubg.png" );
    sys->GLOBAL.buttonBackground     = Pan_TryLoadTexture( sys, "assets/graphics/buttonbg.png" );
    sys->GLOBAL.cursor               = Pan_TryLoadTexture( sys, "assets/graphics/cursor.png" );
    sys->GLOBAL.menuIcons            = Pan_TryLoadTextureTable( sys, "assets/graphics/icons", 16, 16, "png" );

    Pan_MusicVolume( sys, 128/2 );
}

void Teardown( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    (*(sys->currentState.Teardown))( sys );

    Pan_FreeFont( sys, &sys->GLOBAL.largeFont );
    Pan_FreeFont( sys, &sys->GLOBAL.mediumFont );
    Pan_FreeFont( sys, &sys->GLOBAL.smallFont );

    Pan_FreeTexture( sys, &sys->GLOBAL.menuBackground );
    Pan_FreeTexture( sys, &sys->GLOBAL.buttonBackground );
    Pan_FreeTexture( sys, &sys->GLOBAL.cursor );
    Pan_FreeTextureTable( sys, &sys->GLOBAL.menuIcons );

    Pan_Teardown( sys );
}

void HandleInput( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    Pan_HandleInput( sys );
    (*(sys->currentState.HandleInput))( sys );
}

void Update( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    Pan_Update( sys );
    (*(sys->currentState.Update))( sys );
}

void Draw( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    Pan_ClearScreen( sys );
    (*(sys->currentState.Draw))( sys );
    Pan_DrawScreen( sys );
}

void CycleBegin( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    Pan_CycleBegin( sys );
    Pan_LogValueString( sys, __FILE__, __func__, __LINE__, VERBOSE, "Next state", sys->nextState );
    // Check for state change
    if ( strcmp( sys->nextState, "" ) != 0 )
    {
        if ( sys->currentState.Teardown != NULL )
        {
            (*(sys->currentState.Teardown))( sys );
        }
        if      ( strcmp( sys->nextState, "TitleState"              ) == 0 ) { State_Title_Init( sys, &sys->currentState ); }
        else if ( strcmp( sys->nextState, "OptionsState"            ) == 0 ) { State_Options_Init( sys, &sys->currentState ); }
        else if ( strcmp( sys->nextState, "StoreState"              ) == 0 ) { State_Store_Init( sys, &sys->currentState ); }
        else if ( strcmp( sys->nextState, "PickinSticks_TitleState" ) == 0 ) { State_PickinSticks_Title_Init( sys, &sys->currentState ); }
        else if ( strcmp( sys->nextState, "PickinSticks_GameState"  ) == 0 ) { State_PickinSticks_Game_Init( sys, &sys->currentState ); }
        else if ( strcmp( sys->nextState, "MapEditor_State"         ) == 0 ) { State_MapEditor_Init( sys, &sys->currentState ); }

        strcpy( sys->nextState, "" );

        (*(sys->currentState.Setup))( sys );
    }
    (*(sys->currentState.CycleBegin))( sys );
}

void CycleEnd( struct SYSTEM* sys )
{
    Pan_LogFunctionBeginEnd( sys, __FILE__, __func__, __LINE__, "FUNCTION BEGIN" );
    Pan_CycleEnd( sys );
    (*(sys->currentState.CycleEnd))( sys );
}

