#ifndef _FUNCTIONS
#define _FUNCTIONS

#include <stdbool.h>

#include "../omelet_engine/Structures.h"

void Setup( struct SYSTEM* sys );
void Teardown( struct SYSTEM* sys );
void HandleInput( struct SYSTEM* sys );
void Update( struct SYSTEM* sys );
void Draw( struct SYSTEM* sys );
void CycleBegin( struct SYSTEM* sys );
void CycleEnd( struct SYSTEM* sys );

void SetRandomPosition( struct SYSTEM* sys, struct Rect* rect );

#endif
