# Lol I need to learn to actually create makefiles :P

# SOURCES = ../omelet_engine/main_sdl.c
# SOURCES += ../omelet_engine/pan_system.c ../omelet_engine/Utilities.c
# SOURCES += ../omelet_game/OmeletBase.c
# SOURCES += ../omelet_game/States/GameState.c ../omelet_game/States/OptionsState.c ../omelet_game/States/TitleState.c

# EMS += -s USE_SDL=2 -s WASM=1
# EMS += -s ALLOW_MEMORY_GROWTH=1
# EMS += -s DISABLE_EXCEPTION_CATCHING=1 -s NO_EXIT_RUNTIME=0
# EMS += -s ASSERTIONS=1

# CFLAGS = -Wall -std=c17 -DTARGET_SDL

thispath=$PWD
echo $thispath

echo -e "\n\n * USE THE LATEST VERSION OF EMCC!"
#cd ~/emsdk/
#./emsdk install latest
#./emsdk activate latest
#source ./emsdk_env.sh
source /home/rachelwil/emsdk/emsdk_env.sh
emcc -v

echo -e "\n\n * BACK TO THE BUILD DIRECTORY"
#cd $thispath
pwd

echo -e "\n\n * CLEAR PREVIOUS FILES!"
rm *.html *.js *.wasm
rm obj/ -r
rm bin/ -r

echo -e "\n\n * BUILD THE PROJECT!"
emcc -o game.html ../omelet_engine/main_emscripten.c ../omelet_engine/pan_system.c ../omelet_engine/Utilities.c \
    ../omelet_game/OmeletBase.c ../omelet_game/States/GameState.c ../omelet_game/States/OptionsState.c ../omelet_game/States/TitleState.c \
    -s USE_SDL=2 -s -sUSE_SDL=2 -s USE_SDL_IMAGE=2 -s SDL2_IMAGE_FORMATS='["png"]' -s USE_SDL_TTF=2 -s USE_SDL_MIXER=2 \
    -s WASM=1 \
    -D TARGET_SDL -D TARGET_EMSCRIPTEN
